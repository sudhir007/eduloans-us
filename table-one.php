<section class="colored-section">
<div class="container">
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th>Criterias</th>          

                <th> <img src="images/partner/SallieMae.jpg" /> </th>
                <th> <img src="images/partner/ascent_logo.png" /> </th>
                <th> <img src="images/partner/commanbond.png" />  </th>
                <th> <img src="images/partner/earnest_logo.png" />   </th> 
                <th> <img src="images/partner/citizens-bank.png" />   </th> 
                <th> <img src="images/partner/discover.png" />   </th> 
                <th> <img src="images/partner/lendkey.png" />   </th>  
                <th> <img src="images/partner/laurel-road.jpg" />   </th>
                <th> <img src="images/partner/pnc.png" />   </th> 
                <th> <img src="images/partner/sofi.jpg" />  </th>	

           </tr>
        </thead>

        <tbody>
            <tr>
                <td>Description</td>
                <td>Sallie Mae is a leading bank which has the largest marketshare in Education Loans </td>
                <td>Goal Solutions, Inc. (Goal) and Richland State Bank (RSB) created Ascent Student Loans to help revolutionize the way people pay for college. </td>
                <td>CommonBond is a marketplace lender that refinances graduate and undergraduate student loans for university graduates. </td>
                <td>Earnest is a Technology company looking to make Credit easy for Students. They work with multiple funds and lending partners.  </td>
                <td>Citizesn is the 13th largest retail bank in the United States delivering all financial solutions including Student Loans </td>
                <td>Discover Student Loans are made by Discover Bank, a trusted financial institution for 100 years. Discover Bank offers a variety of financial products, including FDIC-insured  </td>
                <td>LendKey connects borrowers with its network of community and regional banks and credit unions. It manages the application, support and servicing of student loans, while the capital for the loan comes from the financial institution. </td>
                <td>A division of KeyBank, online lender Laurel Road having licenses in all 50 states of the US.   </td>
                <td>PNC was chartered in 1845 and has offered private student loans for more than 50 years. </td>
                <td>Social Finance, Inc. is an American online personal finance company that provides student loan refinancing, mortgages and personal loans. </td>
            </tr>

            <tr>
                <td>Year of Establishment</td>
                <td>1973 </td>
                <td>2001 </td>
                <td>2012 </td>
                <td>2013 </td>
                <td>1828 </td>
                <td>2010 </td>
                <td>2007 </td>
                <td>2013 </td>
                <td>1970 </td>
				<td>2011</td>
            </tr>

            <tr>
                <td>Total Loan Book Size</td>
                <td>$22.1 Billion </td>
                <td>$26 Billion </td>
                <td>$3 Billion </td>
                <td>$4.5 billion </td>
                <td>$10 Billion+ </td>
                <td>$8.4 Billion </td>
                <td>$3.1 Billion </td>
                <td>$4 Billion+ </td>
                <td>$5 Billion+ </td>
                <td>$18 billion </td>
            </tr>


            <tr>
                <td>Loan types</td>
                <td>Undergraduate, Career Training, Parent Loans, K12 Loans, MBA, Medical School, Medical Residency, Dental School, Dental Residency, Law School, Bar Study, Graduate </td>
                <td>All Graduate/ Undergraduate studentswith atleast half-time enrolled in a degree program at an eligible institution. </td>
                <td>Undergraduate, graduate, refinancing, MBA, medical </td>
                <td>Undergraduate, graduate, MBA, law, dental, medical, international, refinancing, parent refinancing </td>
                <td>Undergrad, graduate, MBA, law, health care, parent loans </td>
                <td>Undergrad, graduate, MBA, law, dental, health care, international, refinancing </td>
                <td>Undergrad, graduate, parent, refinancing </td>
                <td>Graduate, residents, fellows, parent, refinance </td>
                <td>Undergraduate, graduate, MBA, law, dental, medical, health professions residency, bar study, refinancing </td>
                <td>Social Finance, Inc. is an American online personal finance company that provides student loan refinancing, mortgages and personal loans.</td>
            </tr>

            <tr>
                <td>Interest Rates</td>
                <td>Variable: 2.75% - 10.65%  </td>
                <td>Variable: 3.14% - 11.88%* Fixed:4.09%- 13.03% * </td>
                <td>"Variable: 1.76 % - 5.84% Fixed: 2.93 % - 5.95%"  </td>
                <td>Variable: 2.74% ( Starting) fixed : 4.39% ( Starting)  </td>
                <td>"2.72% APR Variable 4.72% APR Fixed" </td>
                <td>"Variable:  2.80% - 11.37% Fixed : 4.74% - 12.74%" </td>
                <td>Variable: 3.12% (Starting) Fixed :  4.86% (Starting) </td>
                <td>Variable: 4.39% - 8.59% Fixed: 4.39% - 8.68% </td>
                <td>Variable: 4.59% - 11.09% Fixed: 4.49% - 4.99% </td>
                <td>Variable: 3.20%  -  11.12% Fixed: 4.73%  -  11.71% </td>
            </tr>


            <tr>
                <td>Rate types</td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
                <td>Fixed and Variable </td>
            </tr>


            <tr>
                <td>Loan terms </td>
                <td>Five to 15 years	 </td>
                <td>Flexible 5-year, 10-year or 15-year </td>
                <td>Five, 10 or 15 years for most loan types </td>
                <td>Five to 15 years </td>
                <td>Five to 15 years </td>
                <td>15 to 20 years </td>
                <td>10 years </td>
                <td>Five to 20 years </td>
                <td>10 to 15 years </td>
                <td>Five to 20 years</td>             
            </tr>

             <tr>
                <td>Loan amounts</td>
                <td>$1000  up to the cost of attendance </td>
                <td>$2,000 to $200,000 </td>
                <td>$2,000 up to the cost of attendance </td>
                <td>$5,000 to school-certified cost of attendance </td>
                <td>$1,000 to $350,000 </td>
                <td>$1,000 to total cost of attendance minus other financial aid </td>
                <td>$2,000 to total cost of attendance </td>
                <td>$5,000 to no max </td>
                <td>$1,000 to $75,000 refinance </td>
                <td>$5,000 to not disclosed</td>
            </tr>

            <tr>
                <td>Application or origination fees</td>
                <td>No Application or Origination fees </td>
                <td>No origination, disbursement, or loan application fees. </td>
                <td>None for undergraduates with cosigners, an origination fee applies for graduates without a co-signer </td>
                <td>No application fee, origination fee not disclosed </td>
                <td>No application fee, origination fee not disclosed </td>
                <td>No application, origination or late fees </td>
                <td>No application fee, origination fee not disclosed </td>
                <td>No application fee, origination fee not disclosed </td>
                <td>None </td>
                <td> No application fee, origination fee not disclosed</td>
            </tr>


            <tr>
                <td>Discounts</td>
                <td>Autopay </td>
                <td>1% cash back, scholarships, a Refer a Friend Program, Autopay </td>
                <td>Autopay </td>
                <td>Autopay </td>
                <td>Loyalty, autopay </td>
                <td>Autopay, cash reward for at least a 3.0 GPA </td>
                <td>Autopay </td>
                <td>Autopay </td>
                <td>Autopay </td>
	            <td>Autopay, additional SoFi loan discount</td>
            </tr>

            <tr>
                <td>Repayment Options </td>
                <td>In-school interest only, in-school fixed, in-school deferment, immediate repayment </td>
                <td>"In-School Interest Only Repayment: Pay Deferred Repayment: $25 Minimum Payment  Options Available" </td>
                <td>In-school interest only, in-school fixed, in-school deferment, immediate repayment </td>
                <td>Yes, options available </td>
                <td>Not disclosed </td>
                <td>  </td>
                <td>Enrollment, economic hardship </td>
                <td> </td>
                <td>Yes, options available </td>
                <td>Yes, options available </td>
            </tr>

            <tr>
                <td>Deferment or forbearance hardship options </td>
                <td>Deferment option available </td>
                <td>Deferment and Forgiveness available </td>
                <td>Yes </td>
                <td>No, refinance only </td>
                <td>Economic hardship, military service, post active duty, enrolled or returning to school </td>
                <td>Deferment or forbearance available </td>
                <td>May be available after 12 to 36 consecutive on-time payments of principal and interest </td>
                <td>Up to 12-month forbearance for qualified hardships such as involuntary job loss or unpaid maternity leave </td>
                <td>Yes </td>
                <td>None specified </td>
            </tr>

                 <tr>
                <td>Co-signer release ( Months)  </td>
                <td>Can apply for Co-signor release after making 12 payments. Full and on Time </td>
                <td>Can apply for cosigner  release after making the 24 consecutive, regularly scheduled full principal and interest payments on-time  </td>
                <td>After 24 payments in full and on time </td>
                <td>No Cosignor release </td>
                <td>Can apply for a co-signer release after 36 consecutive on-time principal and interest payments </td>
                <td>NA </td>
                <td>No Cosignor release </td>
                <td>Can apply to assume full responsibility of the loan after 36 consecutive on-time payments of principal and interest</td>
                <td>No Cosignor release </td>
                <td>No Cosignor release </td>
            </tr>



            <tr>
                <td>BBB rating</td>
                <td>A+ </td>
                <td>A+ </td>
                <td>A- </td>
                <td>A+ </td>
                <td>A+ </td>
                <td>A+ </td>
                <td>A+ </td>
                <td>A+ </td>
                <td>A+ </td>
                <td>Not Accredited</td>
            </tr>
 
        </tbody>
    </table>
</div>
</div>
</section>
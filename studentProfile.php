<?php include "header.html" ?>

<style type="text/css">

.my_profile{    float: left;
    width: 100%;
    margin: 20px 0;
}
.pro_left_ii .nav-tabs>li.active>a{     background: #f67932;
    border: none;
    color: #fff;
           border: 1px solid #ee6c30;
}
.pro_left_ii  .nav-tabs>li>a {
    border: 1px solid #e3e3e3;
}
.my_profile .media-body,
.my_profile h4,
.my_profileh5{    display: inherit;
    width:100%;
}

.form_steps_tabs_slider li {
        width: 15% !important;
        text-align: center !important;
            margin-left: 2% !important;
}
</style>

<div class="container">

        <div class="row" style="text-align: center;border-radius: 10px;box-shadow: 0px 5px 8px 0px rgba(0,0,0,0.08);padding: 40px 74px;margin-top: -230px;background-color: #fff;">

                <div class="col-md-12">

                        <div class="my_profile">
                                <div class="col-md-3">
                                        <div class="well" style="background: #fff;">
                                                <div class="media text-center">
                                                  <div class="media-center media-bottom">

                                                        <img src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/profile-pic-male_2fd3e8.svg" class="media-object" style="width:80px; margin:0 auto;">
                                                  </div>
                                                  <div class="media-body">

                                                        <h3 style="width:100%" class="media-heading"><span id="user_name">Deepika G.</span></h3>
                                                        <h5 style="width:100%" ><span id="user_email">deepika@gmail.com</h5>
                                                        <h5 style="width:100%" ><span id="user_mobile">9960114422</h5>
                                                        <hr style="margin-top: 10px;margin-bottom: 10px;">
                                                        <ul style="list-style-type:none; margin-left: -50px;">
                                                        <li><b>Req. Loan Amount</b> : $<span id="user_loan_amount">3500 </li>
                                                        <li><b>Counsellor</b> : <span id="facilitator_name">Banit Sawhney</li>
                                                        <li><b>mobile</b> : <span id="facilitator_mobile">12345</li>
                                                        </ul>

                                                  </div>
                                                </div>
                                        </div>

                                        <!--<div class="well pro_left_ii" style="background: #fff;">
                                                <ul class="nav nav-tabs  nav-stacked">
                                                  <li class="active"><a data-toggle="tab" href="#home">MY APPLICATIONS</a></li>
                                                  <li><a data-toggle="tab" href="#menu1">ACCOUNT SETTINGS</a></li>
                                                  <li><a data-toggle="tab" href="#menu2">Logout</a></li>
                                                </ul>

                                        </div>-->


                                </div>
                                <div class="col-md-9">
                                        <div class="tab-content">
                                          <div id="home" class="tab-pane fade in active">
                                                        <div class="well" style="background:#fff;">
                                                        <h4>My Applications</h4>
                                                <hr>
                                                <table class="table table-hover shopping-cart-wrap">
                                                <thead class="text-muted">
                                                <tr >
                                                  <th scope="col"  width="120" style="text-align: center;">Bank</th>
                                                   <th scope="col" style="text-align: center;">Intrest Rate</th>
                                                   <th scope="col" style="text-align: center;">Processing Fees</th>
                                                   <th scope="col" style="text-align: center;">Country</th>
                                                  <th scope="col" width="100" style="text-align: center;">Status</th>


                                                </tr>
                                                </thead>
                                                <tbody id="application_list">

                                                <tr>
                                                        <td style="background-color:white !important;">
                                                                <figure class="media">
                                                                        <div class="img-wrap"><img style="max-height:70px;" src="https://www.eduloans.org/images/partners/ascent_logo.png" class="img-sm"></div>
                                                                </figure>
                                                        </td>
                                                        <td data-label="Intrest Rate">Variable with ACH: 2.46%- 12.98%, Fixed with ACH : 3.39%- 14.50%</td>
                                                        <td data-label="Processing Fees">No application fees or disbursement fees.</td>

                                                        <td>
                                                             United States
                                                        </td>
                                                        <td>
                                                              <a href="https://ascentstudentloans.com/options-consigned-and-no-cosigner/?utm_source=partner&amp;utm_campaign=EduLoans_Rev&amp;utm_medium=EduLoans_Rev_Apply&amp;utm_content=EduLoans_Rev_Learn_More" class="btn btn-md btn-primary" style="display: none;" id="ascentapply" onclick="appliedProduct(21)" target="_blank"><b>Apply</b></a>
                                                              <a href="https://ascentstudentloans.com/options-consigned-and-no-cosigner/?utm_source=partner&amp;utm_campaign=EduLoans_Rev&amp;utm_medium=EduLoans_Rev_Apply&amp;utm_content=EduLoans_Rev_Learn_More" class="btn btn-md btn-warning" style="display: none;" id="ascentreapply" target="_blank"><b>Re-Apply</b></a>
                                                        </td>

                                                </tr>

                                                <tr>
                                                        <td style="background-color:white !important;">
                                                                <figure class="media">
                                                                        <div class="img-wrap"><img style="max-height:70px;" src="https://www.eduloans.org/images/partners/sallieMae_logo.png" class="img-sm"></div>
                                                                </figure>
                                                        </td>
                                                        <td data-label="Intrest Rate">Competitive variable and fixed interest rates</td>
                                                        <td  data-label="Processing Fees">No origination fee or prepayment penalty</td>
                                                        <td>
                                                             United States
                                                        </td>
                                                        <td>
                                                          <a href="https://www.salliemae.com/landing/student-loans/?MPID=3000000203&dtd_cell=SMLRSOPANLNLOTOTAJT1077N010000" class="btn btn-md btn-primary" id="salliemaeapply" onclick="appliedProduct(22)" target="_blank"><b>Apply</b></a>
                                                          <a href="https://www.salliemae.com/landing/student-loans/?MPID=3000000203&dtd_cell=SMLRSOPANLNLOTOTAJT1077N010000" class="btn btn-md btn-warning" style="display: none;" id="salliemaereapply" target="_blank"><b>Re-Apply</b></a>
                                                        </td>

                                                </tr>

                                                <tr>
                                                        <td style="background-color:white !important;">
                                                                <figure class="media">
                                                                        <div class="img-wrap"><img style="max-height:70px;" src="https://www.eduloans.org/images/partners/earnest_logo.png" class="img-sm"></div>
                                                                </figure>
                                                        </td>
                                                        <td data-label="Intrest Rate">Variable rates starting at 2.74% APR (including 0.25% Auto Pay discount).)</td>
                                                        <td  data-label="Processing Fees">No fees for origination, disbursement, prepayment or late payment</td>

                                                        <td>
                                                             United States
                                                        </td>
                                                        <td>
                                                          <a href="https://partner.earnest.com/referral/b266efff-5a50-4693-9bfc-790f3e182666?utm_source=eduloans&amp;utm_medium=referral" class="btn btn-md btn-primary" id="earnestapply" onclick="appliedProduct(23)" target="_blank"><b>Apply</b></a>
                                                          <a href="https://partner.earnest.com/referral/b266efff-5a50-4693-9bfc-790f3e182666?utm_source=eduloans&amp;utm_medium=referral" class="btn btn-md btn-warning" style="display: none;" id="earnestreapply" target="_blank"><b>Re-Apply</b></a>
                                                        </td>

                                                </tr>

                                                </tbody>
                                                </table>
                                                </div> <!-- card.// -->
                                          </div>
                                          <div id="menu1" class="tab-pane fade">
                                                <div class="well" style="background:#fff;">
                                                <h4>Change Password</h4>
                                                <hr>
                                                 <label>Current Password</label>
                                                <div class="form-group pass_show">
                                                        <input type="password" value="" class="form-control" placeholder="Current Password">
                                                </div>
                                                   <label>New Password</label>
                                                <div class="form-group pass_show">
                                                        <input type="password" value="" class="form-control" placeholder="New Password">
                                                </div>
                                                   <label>Confirm Password</label>
                                                <div class="form-group pass_show">
                                                        <input type="password" value="" class="form-control" placeholder="Confirm Password">
                                                </div>
                                                <hr>
                                                <div class="form-group pass_show">
                                                        <input type="submit" class="btn btn-info" value="submit">
                                                </div>

                                                </div>
                                          </div>
                                          <!--div id="menu2" class="tab-pane fade">
                                                <h3>Menu 2</h3>
                                                <p>Some content in menu 2.</p>
                                          </div-->
                                        </div>
                                </div>

                        </div>

                </div>



        </div>


        <!-- <div class="row">

                <div class="col-md-12">

                        <div class="inner-main-content-holder">

                                <h2>Why choose Ascent for student loans </h2>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Offers private student loans at competitive rates</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Cover up to 100% college tuition and living expenses</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No origination or application fees</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No prepayment penalties</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Flexible 5-year, 10-year repayment options</div>

                        </div>

                </div>

        </div> -->

</div>

<!-- <div class="inner-middle-bg">

        <h4>Ascent gives students more opportunities to qualify for a loan</h4>

        <button type="button" class="btn-apply-inner">Get Loan</button>

</div> -->
<div class="container">
    <div class="col-md-12">

       <form name="basicform" id="basicform" method="post" action="yourpage.html">
          <ul id="form_steps_tabs_slider" class="slider form_steps_tabs_slider" style="    display: inline-flex;">
             <li class="active forstep1"><a  href="#1a" > <i class="fas fa-user"></i> Personal Information</a> </li>
             <li class="forstep2"><a href="#2a" > <i class="fas fa-hotel"></i> <i class="fas fa-university"></i> School <br> Information</a> </li>
             <li class="forstep3"><a href="#3a" > <i class="fas fa-user-cog"></i> <i class="far fa-id-card"></i> Co-Signer Personal Information</a> </li>
             <li class="forstep4"><a href="#4a" >  <i class="fas fa-briefcase"></i> </i>Co-Signer Employer Information </a> </li>
             <li class="forstep5"><a href="#4a" > <i class="fas fa-building"></i> Co-Signer Apartment Information </a> </li>
             <li class="forstep6"><a href="#4a" >  <i class="far fa-address-book"></i> Alternate contact in USA </a> </li>
          </ul>
          <!--
             <ul class="row nav nav-pills "  id="form_steps_tabs">
               <li class="col-lg-2 col-md-2 col-sm-6 col-6 active"><a  href="#1a" data-toggle="tab">Personal Information</a> </li>
               <li class="col-lg-2 col-md-2 col-sm-6 col-6"><a href="#2a" data-toggle="tab">School Information</a> </li>
               <li class="col-lg-2 col-md-2 col-sm-6 col-6"><a href="#3a" data-toggle="tab">Co-Signer Personal Information</a> </li>
               <li class="col-lg-2 col-md-2 col-sm-6 col-6"><a href="#4a" data-toggle="tab">Co-Signer Employer Information </a> </li>
               <li class="col-lg-2 col-md-2 col-sm-6 col-6"><a href="#4a" data-toggle="tab">Co-Signer Apartment Information </a> </li>
               <li class="col-lg-2 col-md-2 col-sm-6 col-6"><a href="#4a" data-toggle="tab">Alternate contact in USA </a> </li>
             </ul> -->
          <div id="sf1" class="frm" >
             <fieldset>
                <div class="text-center my-section-header">Step 1 of 2 - Personal Information</div>
                <div class="row">
                   <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                         <input type="text" id="name" name="name" class="form-control element-block" placeholder="Name">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="email" id="email" name="email" class="form-control element-block" placeholder="Email">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="phone" maxlength="10" name="phone" class="form-control element-block" placeholder="Mobile No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <select class="form-control element-block" id="type_of_loan" name="type_of_loan">
                            <option value=''><strong>Select Type Of Loan</strong></option>
                            <option value='Private_Student_Loans'><strong>Private Student Loans</strong></option>
                            <option value='Private_Student_Loan_Refinancing'><strong>Private Student Loan Refinancing</strong></option>
                         </select>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="loan_amount" name="loan_amount" class="form-control element-block" placeholder="Loan Amount">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <select class="form-control element-block" id="usState" name="usState">
                            <option value=''><strong> Select State</strong></option>
                         </select>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="yrs_address" name="yrs_address" maxlength="4" class="form-control element-block " placeholder="No. of years at the address">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                         <textarea id="address" name="address" class="form-control element-block " placeholder="Address"></textarea>
                      </div>
                   </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                   <div class="col-md-12">
                      <button class="btn btn-submit-contact open2" type="button">Next <span class="fa fa-arrow-right"></span></button>
                   </div>
                </div>
             </fieldset>
          </div>
          <div id="sf2" class="frm" style="display: none;">
             <fieldset>
                <legend>Step 2 of 2 - School Information</legend>
                <div class="row">
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <select class="form-control element-block" id="usUniversities" name="usUniversities">
                            <option value=''><strong>Select University</strong></option>
                         </select>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <select class="form-control element-block" id="usCourses" name="usCourses">
                            <option value=''><strong>Select Course</strong></option>
                         </select>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="total_cost_of_study" name="total_cost_of_study" class="form-control element-block" placeholder="Total Cost Of Study">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text"  id="durationprogram" name="durationprogram" class="form-control element-block" placeholder="Duration of Program">
                      </div>
                   </div>
                   <!--  <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                        <input type="text" id="Financial" name="Financial" class="form-control element-block" placeholder="Financial Aid provided by the school">
                      </div>
                      </div>
                      <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                        <input type="text" id="financialaid" name="financialaid" class="form-control element-block" placeholder="Any other financial Aid provided">
                      </div>
                      </div>-->
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                   <div class="col-md-12">
                      <button class="btn btn-submit-contact back1" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                      <button class="btn btn-submit-contact submit_data" type="button"> Submit <span class="fa fa-arrow-right"></span></button>
                   </div>
                </div>
             </fieldset>
          </div>
          <div id="sf3" class="frm" style="display: none;">
             <fieldset>
                <legend>Step 3 of 6 - Co-Signer Personal Information</legend>
                <div class="row">
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="Co_name" name="Co_name" class="form-control element-block" placeholder="Name">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_date" name="Co_date" class="form-control element-block" id="date" name="date" placeholder="DOB (dd/mm/yyyy)">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="email" id="Co_email" name="Co_email" class="form-control element-block" placeholder="Email">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="Co_phone" name="Co_phone" class="form-control element-block" placeholder="Mobile Phone No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="Co_home_phone" name="Co_home_phone" class="form-control element-block" placeholder="Home Phone no.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="Co_work_phone" name="Co_work_phone" class="form-control element-block" placeholder="Work Phone No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_yrs_address" name="Co_yrs_address" class="form-control element-block"  placeholder="No. of years at the address">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_social_security" name="Co_social_security" class="form-control element-block"  placeholder="Social Security No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_green_card" name="Co_green_card" class="form-control element-block"  placeholder="Green Card/Citizen">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_relationship" name="Co_relationship" class="form-control element-block"  placeholder="Relationship with Borrower">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_year_living" name="Co_year_living" class="form-control element-block"  placeholder="Year of living in USA">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_Graduation_Degree" name="Co_Graduation_Degree" class="form-control element-block"  placeholder="Graduation Degree">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_passing_year" name="Co_passing_year" class="form-control element-block"  placeholder="Passing year">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="Co_Years_achieving" name="Co_Years_achieving" class="form-control element-block"  placeholder="Years of achieving green card/Citizenship">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div id="file-upload-form">
                         <input id="file-upload" type="file" name="fileUpload" />
                         <label for="file-upload" id="file-drag">
                         Select a Passport to upload
                         <br />OR
                         <br />Drag a Passport into this box
                         <br /><br /><span id="file-upload-btn" class="button">Add Passport</span>
                         </label>
                         <!-- <progress id="file-progress" value="0">
                            <span>0</span>%
                            </progress> -->
                         <output for="file-upload" id="messages"></output>
                      </div>
                      <?php
                         //$fn = (isset($_SERVER['HTTP_X_FILE_NAME']) ? $_SERVER['HTTP_X_FILE_NAME'] : false);
                         //$targetDir = 'tmp/';
                         //if ($fn) {
                         //if (isFileValid($fn)) {
                         // AJAX call
                         //file_put_contents(
                         //$targetDir . $fn,
                         //file_get_contents('php://input')
                         //);
                         //removeFile($fn);
                         //}
                         //}
                         //function removeFile($file) {
                         //unlink($targetDir . $file);
                         //}
                         ?>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div id="file-upload-form">
                         <input id="file-upload" type="file" name="fileUpload" />
                         <label for="file-upload" id="file-drag">
                         Select a Driving License to upload
                         <br />OR
                         <br />Drag a Driving License into this box
                         <br /><br /><span id="file-upload-btn" class="button">Add Driving License</span>
                         </label>
                         <!-- <progress id="file-progress" value="0">
                            <span>0</span>%
                            </progress> -->
                         <output for="file-upload" id="messages"></output>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                         <textarea class="form-control element-block" id="Co_Permanent_address" name="Co_Permanent_address" placeholder="Permanent address in USA"></textarea>
                      </div>
                   </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                   <div class="col-md-12">
                      <button class="btn btn-submit-contact back2" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                      <button class="btn btn-submit-contact open4" type="button">Next <span class="fa fa-arrow-right"></span></button>
                   </div>
                </div>
             </fieldset>
          </div>
          <div id="sf4" class="frm" style="display: none;">
             <fieldset>
                <legend>Step 4 of 6 - Co-Signer Employer Information</legend>
                <div class="row">
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="Name_of_Employer" name="Name_of_Employer" class="form-control element-block" placeholder="Name of Employer">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="Employer_Contact_No" name="Employer_Contact_No" class="form-control element-block" placeholder="Employer Contact No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Length_of_Employment" name="Length_of_Employment" class="form-control element-block"  placeholder="Length of Employment">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div id="file-upload-form">
                         <input id="file-upload" type="file" name="fileUpload" />
                         <label for="file-upload" id="file-drag">
                         Select a Appointment Letter to upload
                         <br />OR
                         <br />Drag a Appointment Letter into this box
                         <br /><br /><span id="file-upload-btn" class="button">Add Appointment Letter</span>
                         </label>
                         <!-- <progress id="file-progress" value="0">
                            <span>0</span>%
                            </progress> -->
                         <output for="file-upload" id="messages"></output>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div id="file-upload-form">
                         <input id="file-upload" type="file" name="fileUpload" />
                         <label for="file-upload" id="file-drag">
                         Select a Last six months’ pay slips/Pay stubs to upload
                         <br />OR
                         <br />Drag a Last six months’ pay slips/Pay stubs into this box
                         <br /><br /><span id="file-upload-btn" class="button">Add Pay Slips/Pay Stubs</span>
                         </label>
                         <!-- <progress id="file-progress" value="0">
                            <span>0</span>%
                            </progress> -->
                         <output for="file-upload" id="messages"></output>
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="Co_Occupation" name="Co_Occupation" class="form-control element-block"  placeholder="Occupation:  - Engineer / Nurse etc">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-4">
                      <div class="form-group">
                         <input type="text" id="Co_gross_income" name="Co_gross_income" class="form-control element-block"  placeholder="Gross Income from Employment ">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-2">
                      <div class="form-group">
                         <input type="text" id="Oc_other_income" name="Oc_other_income" class="form-control element-block"  placeholder="Other Income">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                         <textarea class="form-control element-block" id="address_Employer" name="address_Employer" placeholder="Address of Employer"></textarea>
                      </div>
                   </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                   <div class="col-md-12">
                      <button class="btn btn-submit-contact back3" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                      <button class="btn btn-submit-contact open5" type="button">Next <span class="fa fa-arrow-right"></span></button>
                   </div>
                </div>
             </fieldset>
          </div>
          <div id="sf5" class="frm" style="display: none;">
             <fieldset>
                <legend>Step 5 of 6 - Co-Signer Apartment Information</legend>
                <div class="row">
                   <div class="col-xs-12 col-sm-3">
                      <ul class="nav nav-pills nav-stacked">
                         <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #777777;">Apartment<span class="caret"></span></a>
                            <ul class="dropdown-menu my-dropdown-options">
                               <li><a href="#">Rent</a></li>
                               <li><a href="#">Fully Owned</a></li>
                               <li><a href="#">Mortgage</a></li>
                            </ul>
                         </li>
                      </ul>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Co_Mortgage" name="Co_Mortgage" class="form-control element-block"  placeholder="Mortgage payment details">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Oc_Outstanding" name="Oc_Outstanding" class="form-control element-block"  placeholder="Outstanding Mortgage Loan">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="Oc_Monthly_Property_tax" name="Oc_Monthly_Property_tax" class="form-control element-block"  placeholder="Monthly Property Tax">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="Oc_Monthly_Home_Owner" name="Oc_Monthly_Home_Owner" class="form-control element-block"  placeholder="Monthly Home Owner Association Fees">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="Oc_Name_mortgage" name="Oc_Name_mortgage" class="form-control element-block"  placeholder="Name of mortgage lender ">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                         <textarea class="form-control element-block"  id="Permanent_address_usa" name="Permanent_address_usa" placeholder="Permanent address in USA"></textarea>
                      </div>
                   </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                   <div class="col-md-12">
                      <button class="btn btn-submit-contact back4" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                      <button class="btn btn-submit-contact open6" type="button">Next <span class="fa fa-arrow-right"></span></button>
                   </div>
                </div>
             </fieldset>
          </div>
          <div id="sf6" class="frm" style="display: none;">
             <fieldset>
                <legend>Step 6 of 6 - Alternate contact in USA</legend>
                <p>2 people one of them has to be a relative (Prefer green card holder/Citizen but H1B would also work)</p>
                <div class="row">
                   <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                         <input type="text" id="usa_name" name="usa_name" class="form-control element-block" placeholder="Name">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="email" id="usa_email" name="usa_email" class="form-control element-block" placeholder="Email">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="usa_phone" name="usa_phone" class="form-control element-block" placeholder="Mobile Phone No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="usa_home_phone" name="usa_home_phone" class="form-control element-block" placeholder="Home Phone no.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="tel" id="usa_work_phone" name="usa_work_phone" class="form-control element-block" placeholder="Work Phone No.">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="usa_yrs_address" name="usa_yrs_address" class="form-control element-block"  placeholder="Year of living in USA">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-3">
                      <div class="form-group">
                         <input type="text" id="usa_yrs_achieving" name="usa_yrs_achieving" class="form-control element-block"  placeholder="Years of achieving green card/Citizenship">
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                         <textarea class="form-control element-block" id="usa_Permanent_address" name="usa_Permanent_address" placeholder="Permanent address in USA"></textarea>
                      </div>
                   </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                   <div class="col-md-12">
                      <button class="btn btn-submit-contact back5" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                      <button class="btn btn-submit-contact submit_data" type="button">Submit </button>
                      <img src="images/spinner.gif" alt="" id="loader" style="display: none">
                   </div>
                </div>
             </fieldset>
          </div>
       </form>

    </div>
    <!-- offers satrt -->
    <div class="col-md-12 offers">
    <h2>offers as per your given data</h2>
    <table id="example" class="display" style="width:100%">
       <thead>
          <tr>
             <th scope="col">Bank Name</th>
             <th scope="col">Intrest Rate</th>
             <th scope="col">Processing Fees</th>
             <th scope="col">Processing Time</th>
             <th scope="col">Max Loan Offered</th>
             <th>action</th>
          </tr>
       </thead>
       <tbody id="Private_Student_Loan_Refinancing_Offer">
          <tr>
             <td data-label="Bank Name"><img src="images/logos/ascent_logo.png" alt="Ascent Student Loans" class="img-fluid"></td>
             <td data-label="Intrest Rate">Variable with ACH: 2.46%- 12.98%, Fixed with ACH : 3.39%- 14.50%</td>
             <td data-label="Processing Fees">No application fees or disbursement fees.</td>
             <td data-label="Processing Time">Fast Online Processing.</td>
             <td data-label="Max Loan Offered">100% of I20 - $1,000 up to $200,000 or total cost of attendance less aid received).</td>
             <td> <a onclick="appliedProduct(21)" href="https://ascentstudentloans.com/options-consigned-and-no-cosigner/?utm_source=partner&amp;utm_campaign=EduLoans_Rev&amp;utm_medium=EduLoans_Rev_Apply&amp;utm_content=EduLoans_Rev_Learn_More" target="_blank" class="btn btn-danger "> Apply </a> </td>
          </tr>
          <tr>
             <td data-label="Bank Name"><img src="images/logos/sallieMae_logo.png" alt="Sallie Mae Student Loans" class="img-fluid"></td>
             <td data-label="Intrest Rate">Competitive variable and fixed interest rates</td>
             <td  data-label="Processing Fees">No origination fee or prepayment penalty</td>
             <td data-label="Processing Time"> - </td>
             <td data-label="Max Loan Offered"> - </td>
             <td> <a onclick="appliedProduct(22)" href="https://www.salliemae.com/landing/student-loans/?MPID=3000000203&dtd_cell=SMLRSOPANLNLOTOTAJT1077N010000" target="_blank" class="btn btn-danger "> Apply </a> </td>
          </tr>
          <tr>
             <td data-label="Bank Name"><img src="images/logos/earnest.png" alt="Earnest Student Loans" class="img-fluid"></td>
             <td data-label="Intrest Rate">Variable rates starting at 2.74% APR (including 0.25% Auto Pay discount).)</td>
             <td  data-label="Processing Fees">No fees for origination, disbursement, prepayment or late payment</td>
             <td data-label="Processing Time">Fast application and decision-making process.</td>
             <td data-label="Max Loan Offered">Covers up to 100% of the school's certified cost of attendance</td>
             <td> <a onclick="appliedProduct(23)" href="https://partner.earnest.com/referral/b266efff-5a50-4693-9bfc-790f3e182666?utm_source=eduloans&amp;utm_medium=referral" target="_blank" class="btn btn-danger "> Apply </a> </td>
          </tr>
       </tbody>
    </table>
    <a href="studentProfile.php" class="btn btn-submit-contact" >
    Finish
    </a>
    </div>
    <!-- offers end -->
</div>

<?php //include "table.php" ?>
<section class="testimonials-block text-center bg-cover">

        <div class="container">

                <div class="row">

                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">

                                <h2 class="">Our Happy Borrowers</h2>

                                <!-- testimonail slider -->

                                <div class="slider borrower-testimonial-slider borrower-slider-parent">

                                        <div>

                                                <!-- testimonial quote -->

                                                <blockquote class="testimonial-quote">

                                                        <p>“Being young I don’t have a strong credit history. Ascent made it possible for me to have my own loan without the help of others. I’m thankful I found them.”</p>

                                                        <cite class="element-block">

                                                                <strong class="element-block h5 h my-text-color">Nikara M., Western Washington University</span></strong>

                                                        </cite>

                                                </blockquote>

                                        </div>

                                        <div>

                                                <!-- testimonial quote -->

                                                <blockquote class="testimonial-quote">

                                                        <p>“ Trent from punchy rollie grab us a waggin school. Flat out

                                                                like a bludger where he hasn't got a damper. As stands

                                                                out like brass razoo heaps it'll be relo. As busy as a

                                                                paddock.”</p>

                                                        <cite class="element-block ">

                                                                <strong class="element-block h5 h my-text-color">Logan (US) Sallie Mae customer</span></strong>

                                                        </cite>

                                                </blockquote>

                                        </div>

                                        <div>

                                                <!-- testimonial quote -->

                                                <blockquote class="testimonial-quote ">

                                                        <p>“ Trent from punchy rollie grab us a waggin school. Flat out

                                                                like a bludger where he hasn't got a damper. As stands

                                                                out like brass razoo heaps it'll be relo. As busy as a

                                                                paddock.”</p>

                                                        <cite class="element-block ">

                                                                <strong class="element-block h5 h my-text-color">Cynthia O. (Canada) Sallie Mae customer</span></strong>

                                                        </cite>

                                                </blockquote>

                                        </div>

                                        <div>

                                                <!-- testimonial quote -->

                                                <blockquote class="testimonial-quote ">

                                                        <p>“ Trent from punchy rollie grab us a waggin school. Flat out

                                                                like a bludger where he hasn't got a damper. As stands

                                                                out like brass razoo heaps it'll be relo. As busy as a

                                                                paddock.”</p>

                                                        <cite class="element-block ">

                                                                <strong class="element-block h5 h my-text-color">Logan (US) Sallie Mae customer</span></strong>

                                                        </cite>

                                                </blockquote>

                                        </div>

                                </div>

                        </div>

                </div>

        </div>

</section>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">

        //When the page has loaded.
        $( document ).ready(function(){
        //alert("On page load");
            //Perform Ajax request.
            var access_token = $.cookie('access_token');
            $.ajax({

                url: 'https://api.eduloans.org/partner/profile/info',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'source': '5',
                    'access-token' : access_token
                },
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    //  alert("OK");
                   //console.log(data.data_object.email);

                $("#loggedOUT").hide();
                $("#loggedIN").show();

                $('#user_name').html(data.data_object.first_name +' '+ (data.data_object.last_name ? data.data_object.last_name : ''));
                $('#user_email').html(data.data_object.email);
                $('#user_mobile').html(data.data_object.mobile_number);
                $('#user_loan_amount').html(data.data_object.requested_loan_amount);

                //form_fillups
                document.getElementById("name").value = (data.data_object.first_name +' '+ (data.data_object.last_name ? data.data_object.last_name : ''));
                document.getElementById("email").value = (data.data_object.email);
                document.getElementById("phone").value = (data.data_object.mobile_number);
                document.getElementById("loan_amount").value = (data.data_object.requested_loan_amount);

                //facilitator
                $('#facilitator_name').html(data.data_object.facilitator_first_name +' '+ data.data_object.facilitator_last_name);
                $('#facilitator_mobile').html(data.data_object.facilitator_mobile);

                   //window.location.href='/';

              var applicationList = data.application_list_object;
              if(applicationList.length){

                applicationList.forEach(function(value, index){

                  if(value.id === "21" || value.id === 21){

                    $("#ascentapply").hide();
                    $("#ascentreapply").show();

                  }
                  if(value.id === "22" || value.id === 22){

                    $("#salliemaeapply").hide();
                    $("#salliemaereapply").show();

                  }
                  if(value.id === "23" || value.id === 23){

                    $("#earnestapply").hide();
                    $("#earnestreapply").show();

                  }


                });

              }

                },
                  error: function (error) {
                        alert("Error");
                    window.location.href='/';
                }
            });
        });

        function appliedProduct(product_id){

          var user_access = $.cookie('access_token');
          var lead_id = $.cookie('lead_id');

          $.ajax({
                  url: 'https://api.eduloans.org/partner/apply/product',
                  type: 'POST',
                  dataType: 'json',
                  contentType: 'application/json',
                  data: JSON.stringify( {
                    "lead_id" : lead_id,
                    "product_id" : product_id
                  }),
                  headers: {
                      'source': '5',
                      'access-token' : user_access
                  },
                  contentType: 'application/json; charset=utf-8',
                  success: function (result) {

                    alert("Application SuccessFully Applied...!!!");
                     //console.log(result);
                     //window.location.href='/';

                  },
                    error: function (error) {
                    //  window.location.href='/';
                  }
              });

        }

</script>

<?php include "footer.html" ?>

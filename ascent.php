<?php include "header.html" ?>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                <p class="my-breadcrumbs">Partner with us / Ascent</p>

                                <h1>Ascent</h1>

                                <p>Ascent is one of the leading student loan providers that has helped thousands of students nationwide achieve their goals of paying for school tuition. As a student-focused lender, Ascent provides distinct private loan products to match a school’s precise funding needs. Launched in 2017, this US-based student loan provider company offers student loans to borrowers in all 50 states.

                                </p>

                                <p>

                                	Ascent understands that every student’s situation is different, and created private student loan programs that give students more opportunities to qualify for a loan with or without a cosigner. It offers two student loan options- cosigned and non-cosigned private student loans for undergraduates. Those who can’t qualify for a loan in their name can go for the cosigned loan. The cosigned loan is good for borrowers who want to use a cosigner and pay off loans fast with lower interest rates. Ascent also offers benefits such as 1% cashback, scholarships, a Refer a Friend Program, a Rewards Program, and more.

                                </p>

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-12">

                        <div class="inner-main-content-holder">

                                <h2>Why choose Ascent for student loans </h2>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Offers private student loans at competitive rates</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Cover up to 100% college tuition and living expenses</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No origination or application fees</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No prepayment penalties</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Flexible 5-year, 10-year repayment options</div>

                        </div>

                </div>

        </div>

</div>

<div class="inner-middle-bg">

        <h4>Ascent gives students more opportunities to qualify for a loan</h4>

        <button type="button" class="btn-apply-inner">Get Loan</button>

</div>
<?php include "table-two.php" ?>
<?php include "footer.html" ?>
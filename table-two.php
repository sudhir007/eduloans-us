<section class="colored-section">
  <div class="container">

<header class="popular-posts-head">
 <p>From Our Partner Lenders</p>
 <h2 class="popular-head-heading">Compare comprehensive options</h2>
</header>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th>  </th>
                <th> <img src="images/partner/SallieMae.jpg" /> </th>
                <th> <img src="images/partner/ascent_logo.png" /> </th>
                <!-- <th> <img src="images/partner/commanbond.png" />  </th> -->
                <th> <img src="images/partner/earnest_logo.png" />   </th>          
             </tr>
        </thead>
        <tbody>

            <tr>
                <td>Year of Establishment</td>
                <td>2005</td>
                <td>2001</td>
                <!-- <td>2012</td> -->
                <td>2013</td>              
            </tr>

            <tr>
                <td>Student Loan Size</td>
                <td>$22.9 Billion </td>
                <td>$26 Billion </td>
               <!--  <td>$ 3 Billion </td> -->
                <td>$4.5 billion </td>                
            </tr>

            <tr>
                <td>Loan types</td>
                <td>Undergraduate, Career Training, Parent Loans, K12 Loans, MBA, Medical School, Medical Residency, Dental School, 
                	Dental Residency, Law School, Bar Study, Graduate </td>
                <td>All Graduate/Undergraduate studentswith atleast half-time enrolled in a degree program at an eligible institution.</td>
                <!-- <td>Undergraduate, graduate, refinancing, MBA, medical</td> -->
                <td>Undergraduate, graduate, MBA, law, dental, medical, international, refinancing, parent refinancing </td>              
            </tr>

             <tr>
                <td>Rate types</td>
                <td>Fixed and Variable  </td>
                <td>Fixed and Variable  </td>
               <!--  <td>Fixed and variable  </td> -->
                <td>Fixed and variable  </td>             
            </tr>  

             <tr>
                <td>Interest Rates*</td>
                <td>"Variable: 2.75%  -  10.65%, Fixed :4.74%  - 11.85%"  </td>
                <td>Variable: 3.14% - 11.88%*, Fixed:4.09%- 13.03% * </td>
                <!-- <td>"Variable: 1.76 % - 5.84%, Fixed: 2.93 % - 5.95%"  </td> -->
                <td>Variable: 2.74% ( Starting), fixed : 4.39% ( Starting) </td>              
            </tr>
            
              <tr>
                <td>Loan terms (Years)</td>
                <td>5, 10, 15   </td>
                <td>5, 10, 15   </td>
                <!-- <td>5, 10, 15   </td> -->
                <td>5, 10, 15   </td>              
             </tr>            

              <tr>
                <td>Loan amounts</td>
                <td>$1000  up to the cost of attendance  </td>
                <td>$2,000 to $200,000	  </td>
                <!-- <td>$2,000 up to the cost of attendance  </td> -->
                <td>$5,000 to school-certified cost of attendance  </td> 
            </tr>

              <tr>
                <td>Application or origination fees</td>
                <td>No Fees  </td>
                <td>No Fees  </td>
                <!-- <td>No Fees  </td> -->
                <td>No Fees  </td>              
            </tr>

              <tr>
                <td>Discounts</td>
                <td>Autopay  </td>
                <td>Autopay  </td>
               <!--  <td>Autopay  </td> -->
                <td>Autopay  </td>              
            </tr>

              <tr>
                <td>Repayment Options</td>
                <td> - In-school interest only - in-school fixed -in-school deferment - immediate repayment  </td>
                <td>"In-School Interest Only, Pay Deferred Repayment Minimum Payment ($25)"  </td>
                <!-- <td>In-school interest only, in-school fixed, in-school deferment, immediate repayment  </td> -->
                <td>Yes, options available  </td>              
            </tr>

              <tr>
                <td>Deferment or forbearance hardship options</td>
                <td>Available  </td>
                <td>Available  </td>
                <!-- <td>Available  </td> -->
                <td>Available  </td>              
            </tr>

              <tr>
                <td>Co-signer release ( Months plus timely payment) </td>
                <td>12 Months  </td>
                <td>24 Months  </td>
                <!-- <td>24 Months  </td> -->
                <td>No Cosignor release  </td>              
            </tr>

               <tr>
                <td>BBB rating </td>
                <td>A+  </td>
                <td>A+  </td>
                <!-- <td>A+  </td> -->
                <td>A+  </td>              
            </tr>

        </tbody>
    </table>
</div>

<div class="table-bottom-text"> <p>*As per last Update on 3/13/2020 </p> </div>
</div>
</section>

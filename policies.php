<?php include "header.html" ?>




<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                <p class="my-breadcrumbs">Last Updated: January 1, 2020</p>

                                <h1>Privacy Policy</h1>

                                <p>BY ACCESSING OR USING THE SERVICES, YOU AGREE TO THIS POLICY. IF YOU DO NOT AGREE TO THIS POLICY, PLEASE DO NOT USE THE SERVICE.</p>

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-12">

 <div class="inner-main-content-holder">
                              
 <div class="my-marginer"> This Privacy Policy explained on the website by  BITC Corporation. and its subsidiaries (e.g. BITC Corporation, India.) (collectively, “Eduloans,” “Company”, we,” “our” or “us”) collect, use, disclose, and protect information we collect through our online services, websites, mobile applications, and software provided on or in connection with the services and also any and all communications with us via phone, email, text or otherwise (each, a “Service” and collectively, “Services”). Capitalized terms that are not defined in this Privacy Policy have the meaning given to them in our Terms of Service.
When you use the Services, you consent to our collection, use, disclosure, and protection of information about you as described in this Privacy Policy.

 </div>  


<h2> 1. Collection of Your Information</h2>
<p>We may collect and store various information about you including, for example, information that you provide to us, information that we automatically collect through your use of the Service, and information from publicly available sources or third parties, as further detailed below. If you utilize our Services to obtain a financial product or service through us, we will use and share any information that we collect from or about you in accordance with our U.S. Consumer Privacy Notice, which offers you certain choices with respect to the use and sharing of your personal information. Information subject to our Privacy Notice is referred to as Non-Public Personal Information (“NPI”).</p>
<strong> A. Information you provide us directly</strong>
<p>The information we collect from you will vary depending on the product or service requested. We may ask for certain information such as your username, first and last name, birth date, telephone number, email address, college or university, year of graduation, course of study, outstanding loan balances, social security number, income, employment history and other details related to your financial situation when you use our Service and/or register for a Eduloans account. We may collect information that you voluntarily provide to us when using the Service. All of the information that we collect from you directly is identified in the forms, other places where you respond to questions, or where you indicate preferences throughout the Service, either online or communicated through our customer service representatives. </p>
<p>We may also collect and store information that you provide to us about other people. By submitting information about someone other than yourself, you represent that you are authorized to provide us with that person’s information for the purposes identified in this Privacy Policy and/or in connection with the Service. </p>
<p> We may also retain any messages you send through the Service, customer service correspondence (via telephone, chat, text, email or otherwise), and may collect information you provide in User Content you post to the Service. Calls to Eduloans or to third-parties through Eduloans by you or to you may be recorded or monitored for quality assurance, customer service, training and/or risk management purposes.</p>

<strong> B. Information That Is Collected Automatically </strong>
<p>In addition to the information you provide to us directly, we may automatically collect information about your use of the Service, as described below.</p>
<ul>
<li>   Log file information. Log file information is automatically reported by your browser each time you access the Service. When you use our Service, our servers may automatically record certain log file information. These server logs may include information such as your web request, Internet Protocol (“IP”) address, browser type, browser language, operating system, platform type, the state or country from which you accessed the Service, software and hardware attributes (including Device ID), referring / exit pages and URLs, number of clicks and how you interact with links on the Service, domain names, landing pages, pages viewed and the order of those pages, the data and time you used the Service and uploaded or posted content, error logs, files you download, and other such information. </li>
<li>   Location information. When you use the Service, we may automatically collect general location information (e.g., IP address, city/state and or zip code associated with an IP address) from your computer or mobile device. Please note that if you disable such features, you may not be able to access or receive some or all of the services, content, features and/or products made available via the Service.</li>

<li> Cookies and other technologies. We and our third-party service providers may use cookies, clear GIFs, pixel tags, beacons, and other technologies that help us better understand user behavior, personalize preferences, perform research and analytics, deliver tailored advertising, and improve the products and services we provide. You can choose to accept or decline certain cookies. Most web browsers automatically accept cookies, but your browser may allow you to modify your browser settings to decline certain cookies if you prefer. If you disable cookies, you may be prevented from accessing or taking full advantage of the Services. </li>
</ul>
<p> <strong>1. Cookies</strong>  When you visit the Service, we may send one or more cookies — a small text file containing a string of alphanumeric characters — to your computer that uniquely identifies your browser or stores information or settings on your device. Cookies let us help you log in faster and enhance your navigation through the site. A cookie may also convey information to us about how you use the Service (e.g., the pages you view, the links you click and other actions you take on the Service), and allow us or our business partners to track your usage of the Service over time. A persistent cookie remains on your hard drive after you close your browser. Persistent cookies may be used by your browser on subsequent visits to the site. Persistent cookies can be removed by following your web browser’s directions. A session cookie is temporary and disappears after you close your browser. Our Service may use HTTP cookies, HTML5 cookies and other types of local storage (such as browser-based or plugin-based local storage).</p>

<p> <strong> 2. Clear gifs/web beacons</strong> When you use the Service, we may employ clear gifs (also known as web beacons) which are used to anonymously track the online usage patterns of our Users. In addition, we may also use clear gifs in HTML-based emails sent to our users to track which emails are opened, which links are clicked by recipients, and whether our emails are forwarded. The information allows for more accurate reporting and improvement of the Service.</p>

<p> <strong> 3. Web analytics </strong> We may use third-party Web analytics services on our Service, such as those of Google Analytics. These service providers use the technology described above to help us analyze how users use the Service. The information collected by the technology (including your IP address) will be disclosed to or collected directly by these service providers. </p>

<p> <strong> 4. Third-Party Ad Targeting</strong> We sometimes use the above technologies for interest-based advertising, which may be based on your device or browser’s signals received across websites or applications, and over time. </p>

<p>We may use this information in a number of ways, including: (a) to remember information so that you will not have to re-enter it during your visit or the next time you visit the site; (b) to provide custom, personalized content and information; (c) to provide our Service and monitor its effectiveness, including on or through our affiliates and/or other third party service providers; (d) to monitor aggregate metrics such as total number of visitors, traffic, and demographic patterns; (e) to diagnose or fix technology problems; (f) to help you efficiently access your information after you sign in and; (g) to deliver advertising to you. </p>

<p> <strong> C. Information from Third-Party Sources</strong>
<p> We may receive information about you from publicly and commercially available sources, as permitted by law, which we may combine with other information we receive from or about you. For example, when you request prequalified rates, we will ask your permission to conduct a soft credit inquiry. Likewise, when you apply for a loan product, either us or a Provider may conduct a hard credit inquiry. When you apply for a mortgage, we will conduct a search of public records to determine the estimated value of your home and you estimated taxes in order to automate the loan application process and make things more convenient and streamlined for you. </p>

<h2> 2. Tailored Advertising </h2>

<p> We may use third-party advertising technologies that allow for the delivery of relevant content and advertising on the Service, as well as on other websites you visit and other applications you use. Please refer to our U.S. Consumer Privacy Notice for more information on your rights to opt-out of the sharing of NPI with non-affiliated third-party advertisers and your rights with respect to the sharing of certain information for marketing purposes with affiliate and non-affiliated third parties. The ads may be based on various factors such as the content of the page you are visiting, information you enter, your searches, demographic data, user-generated content, and other information we collect from you. These ads may be based on your current activity or your activity over time and across other websites and online services and may be tailored to your interests. </p>

<p> Also, third-parties whose products or services are accessible or advertised via the Service may place cookies or other tracking technologies on your computer, mobile phone, or other device to collect information about your use of the Service as discussed above. We may also allow other third-parties (e.g., ad networks and ad servers such as Google Analytics, DoubleClick, Quora Pixels and others) to serve tailored ads to you on the Service, other sites, and in other applications, and to access their own cookies or other tracking technologies on your computer, mobile phone, or other device you use to access the Service. </p>

<p> Please note that an advertiser may ask Eduloans to show an ad to a certain audience of Users (e.g., based on demographics or other interests). In that situation, Eduloans determines the target audience and Eduloans serves the advertising to that audience and only provides anonymous aggregated data to the advertiser. If you respond to such an ad, the advertiser or ad server may conclude that you fit the description of the audience they are trying to reach. </p>

<p> We neither have access to, nor does this Privacy Policy govern, the use of cookies or other tracking technologies that may be placed on your computer, mobile phone, or other device you use to access the Service by non-affiliated, third-party ad technology, ad servers, ad networks or any other non-affiliated third-parties. Those parties that use these technologies may offer you a way to opt out of ad targeting as described below. If you receive tailored advertising on your computer through a web browser, you can learn more about such tailored browser advertising and how you can generally control cookies from being put on your computer to deliver tailored advertising by Google Ads Settings page to opt out of tracking.  </p>

<p> Please note that to the extent advertising technology is integrated into the Service, you may still receive advertisements even if you opt out. In that case, the advertising will not be tailored to your interests. Also, we do not control any of the above opt-out links or whether any particular company chooses to participate in these opt-out programs. We are not responsible for any choices you make using these mechanisms or the continued availability or accuracy of these mechanisms. </p>

<h2> 3. Do Not Track Signals and Similar Mechanisms </h2>

<p> Some Internet browsers may be configured to send “Do Not Track” signals to the online services that you visit. We currently do not respond to “Do Not Track” or similar signals. To find out more about “Do Not Track,” please visit http://www.allaboutdnt.com. </p>
<h2> 4. Use of Your Information </h2>

<p> We may use your information and/or combine any or all elements of your information for a number of purposes, including: </p>

<ul>
<li> To provide you with and manage the Service. We use your information to provide, develop, maintain and improve our Service and the features and functionality of the Service. This may include customization and tailoring the Service features according to the information that you provide. </li>

<li> To process and fulfill a request or other transaction submitted to us We use your information to process or fulfill requests or transactions that you have requested. </li>

<li> To communicate with you. We use the information we collect or receive to communicate directly with you. We may send you emails containing newsletters, promotions and special offers. If you do not want to receive such email messages, you will be given the option to opt out or change your preferences. We also use your information to send you Service-related emails (e.g., account verification, changes/updates to features of the Service, technical and security notices). You may not opt out of Service-related emails. </li>

<li> Provide personalized services, including content, ads, products or services. We may use the information we collect to provide personalized services and advertising to you either through Eduloans or a third party marketing partner, subject to the requirements of the U.S. Consumer Privacy Notice.  </li>

<li> Marketing and Advertising. We may use your information to market and advertise our services and the services of certain third parties we work with.

<li> Unique Identifiers. Sometimes, we (or our service providers) may use the information we collect – for instance, log-in credentials, IP addresses and unique mobile device identifiers – to locate or try to locate the same unique users across multiple browsers or devices (such as smartphones, tablets, or computer), or work with providers that do this, in order to better tailor advertising, content, and features, and provide you with a seamless experience across devices. If you wish to opt out of cross device tracking for purposes of interest-based advertising, you may do so through your Android or Apple device-based settings. </li>

<li> Business Operations. We may use your information for certain operational purposes including, for example, perform audits and quality control.</li>

<li> Research and Analytics. We may use your information for research and analytical purposes, for example, to identify trends and effectiveness of certain marketing campaigns we run. </li>

<li> To protect the rights of Eduloans and others.We may use your information as we believe is necessary or appropriate to protect, enforce, or defend the legal rights, privacy, safety, security, or property of Eduloans, its employees or agents, or other users and to comply with applicable law. </li>

<li> With your consent. We may otherwise use your information with your consent or at your direction. </li>

<li> Surveys, Offers, Sweepstakes. We may use your information to conduct surveys and offer sweepstakes, drawings, and similar promotions through our Offer section. </li>

<li> To aggregate/de-identify data. We may aggregate and/or de-identify information collected through the Service. We may use de-identified and/or aggregated data for any purpose and without any restrictions, including without limitation for research and marketing purposes, and may also share such data with any third parties, including advertisers, promotional partners, sponsors, event promoters, and/or others.</li>
</ul>

<h2> 5. Sharing of your information </h2>

<p> We may share the information we collect or receive from you in a variety of circumstances, including as listed below. Where appropriate, we will limit sharing of your information in accordance with the choices you have provided us in response to our U.S. Consumer Privacy Notice. </p>

<ul>

<li> Providers and co-signers. We may share your information with Providers for the purpose of providing the Service to you such that they may provide you with information about your request for prequalified rates, offers, and information related to other products and services they offer. </li>

<li> You agree that when you submit information to a Provider or third-party through the Service, such submission of your information may also be governed by Provider or third-party privacy and security policies which differ from our Privacy Policy. For details of these policies, please consult the policies of the Providers or third-parties directly. </li>

<li> You understand that, subject to any applicable laws or regulations, Providers may keep your information that relates to any submission of information that you may make through your use of the Service and any other information provided by Eduloans in the processing of any submission of information, whether or not you are eligible for the Provider’s products or services. You agree to notify any particular Provider directly if you no longer want to them to keep your information. </li>

<li> As a prospective borrower or applicant of a Provider’s products or services, you agree that when you nominate a co-signer/co-applicant, Eduloans may share Provider responses relating to your submission with your nominated co-signer/co-applicant, and, as a co-signer/co-applicant, we may share Provider responses relating to your submission with the prospective borrower or applicant. </li>

<li> <strong> Affiliates </strong> We may share information with our parent company and any of our or our parent company’s or subsidiaries.</li>

<li> <strong> Service providers</strong> We may share your personal information with third parties who work on behalf of, or with, us such as vendors, processors, agents and other representatives. Service providers assist us with a variety of functions. This includes, for example, assisting with customer service-related functions, providing advertising (including interest-based advertising) and analytics services (e.g., Google Analytics and Facebook Custom Audiences), helping us verify your identity, obtain your credit report, providing website hosting services (e.g., Amazon Web Services), assisting with auditing functions, and helping us with marketing and related research activities. </li>

<li> <strong> Marketing partners</strong> We may share your personal information with third-party marketing partners including partners who we conduct joint-marketing activities with or who we offer a co-branded service with. Marketing partners also include those entities who maintain a link to our Services on their site, or a Eduloans widget on their site, when you interact with that widget or click from their site to our Services.</li>

<li> <strong> Other parties </strong> when required by law or as necessary to protect our users and services. Eduloans may share your information where required to do so by law or subpoena or if we reasonably believe that such action is necessary to (a) comply with the law and the reasonable requests of law enforcement; (b) enforce our Terms of Service or to protect the security or integrity of our Service; and/or (c) exercise or protect the rights, property, or personal safety of Eduloans, our Users or others.</li>

<li> <strong> Business transfers</strong> We may buy or sell/divest/transfer BITC Corporation (including any shares in BITC Corporation), or any combination of its products, services, assets and/or businesses. Your information, which may include NPI, user names and email addresses, User Content and other user information related to the Service may be among the items sold or otherwise transferred in these types of transactions. We may also sell, assign or otherwise transfer such information in the course of corporate divestitures, mergers, acquisitions, bankruptcies, dissolutions, reorganizations, liquidations, similar transactions or proceedings involving all or a portion of BITC Corporation. </li>

<li> <strong> Public Forums</strong> Any information or content that you voluntarily disclose for posting to the Service, such as User Content, becomes available to the public, as controlled by any applicable privacy settings. Subject to your profile and privacy settings, any User Content that you make public is searchable by other Users. If we remove information that you posted to the Service, copies may remain viewable in cached and archived pages of the Service, or if other Users have copied or saved that information. We are not responsible for the information that you choose to submit in your User Content.

<li> Aggregated or de-identified information. We may also aggregate or de-identify data and may share that data without restriction. </li>

<li> Otherwise with your consent or at your direction. In addition to the sharing described in this Privacy Policy, we may share information about you with third-parties whenever you consent to or direct such sharing. </li>

</ul>

<h2> 6. How We Store and Protect Your Information</h2>
<p> Keeping Your Information Safe Eduloans takes technical and organizational measures to protect your data against accidental or unlawful destruction or accidental loss, alteration, unauthorized disclosure or access. These measures vary depending on the sensitivity of the information we have collected from you. However, no method of transmission over the Internet or via mobile device, or method of electronic storage, is absolutely secure. Therefore, while we strive to use commercially acceptable means to protect your information, we cannot guarantee its absolute security. Eduloans is not responsible for the functionality or security measures of any third-party or Provider.
To protect your privacy and security, we take steps (such as requesting a unique password) to verify your identity before granting you access to your account. You are responsible for maintaining the secrecy of your unique password and account information, and for controlling access to your email communications from Eduloans, at all times. Do not share your password with anyone and do limit your access to your computer or other devices by signing off after you have finished accessing your account. </p>

<p>For information about how to protect yourself against identity theft, please refer to the Federal Trade Commission’s website at www.ftc.gov/news-events/media-resources/identity-theft-and-data-security. </p>

<h2> 7. Your Choices About Your Information</h2>

<strong> A. Your Account Information and Settings </strong>

<p> If you choose to set up a profile, you may update your account information at any time by logging into your account and changing your profile settings. You can also contact us at support@eduloans.org to change your preferences. </p>

<strong> B. Email Promotions </strong>
<p> You can stop receiving promotional email communications from us by clicking on the “unsubscribe link” provided in such communications. We make every effort to promptly process all unsubscribe requests. You may not opt out of Service-related communications (e.g., account verification, transaction-related updates, changes/updates to features of the Service, and technical and security notices). If you have any questions about reviewing or modifying your account information, you can contact us directly at support@Eduloans.com.</p>

<strong> C. Opt-Out and Annual Notice </strong>

<p> If you have a financial product or service with us, we will use and share any information that we collect from or about you in accordance with our U.S. Consumer Privacy Notice, which offers you certain choices with respect to the use and sharing of your personal information, including the right to opt-out to the sharing of certain information.</p>

<strong> D. Choices Relating to Tailored Advertising</strong>
<p> Please see the section above titled, “Tailored Advertising,” for your choices relating to certain interest-based advertising.</p>

<h2> 8. How Long We Keep Your User Content </h2>
<p> Following termination or deactivation of your User account, Eduloans may retain your profile information and User Content for an additional period of time for backup, archival, or audit purposes. In certain circumstances, we are required by Federal and State regulations to store various information. In order to comply with those requirements, we may be unable to delete your information from our database until the legally required retention period expires. </p>

<h2> 9. Children’s Privacy </h2>
<p> Eduloans does not knowingly collect or solicit any information from anyone under the age of 16 or allow such persons to register as Users. In the event that we learn that we have collected personal information from a child under the age of 16, we will promptly delete that information to the best of our ability. If you believe that we might have collected any information from a child under 16, please contact us at support@Eduloans.com </p>

<h2>10. Links to Other Web Sites and Services</h2>

<p> We are not responsible for the practices employed by websites or services linked to or from the Service or to which you direct Eduloans to retrieve information, including the information or content contained therein. Please remember that when you use a link to go from the Service to another website, our Privacy Policy does not apply to third-party websites or services. Your browsing and interaction on any third-party website or service, including those that have a link or advertisement on our website, are subject to that third-party’s own rules and policies. In addition, you agree that we are not responsible for, and we do not have control over, any third-parties that you authorize to access your User Content. If you are using a third-party website or service and you allow such a third-party access to your User Content, you do so at your own risk. Eduloans is also not responsible for sites directing you to Eduloans. Our Terms of Service and Privacy Policy do not apply to those third-party sites either. </p>

<h2> 11. Your California Privacy Rights </h2>
<p> California Civil Code Section 1798.83, also known as the "Shine The Light" law, permits Users who are California residents to request and obtain from us once a year, free of charge, information about the personal information (if any) we disclosed to third-parties for direct marketing purposes in the preceding calendar year. If applicable, this information would include a list of the categories of personal information that were shared and the names and addresses of all third-parties with which we shared information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request via email to support@eduloans.org: The California Consumer Privacy Act (“CCPA”) also affords California residents certain rights over their personal information (subject to certain exceptions). Our CCPA Privacy Notice, which supplements this Privacy Policy, outlines those rights and explains how they may be exercised. </p>

<h2> 12. Complaints and How to Contact Us </h2>
<p> To make a complaint about a privacy-related matter, or to request access to or correction of your personal information, or if you have any questions about this Privacy Policy or the Service, please contact us at support@eduloans.org or by phone at 328-347-8189. We will review all complaints and determine whether the matter needs further investigation. In the event an investigation is conducted concerning your personal information, we will notify you of the outcome of the investigation.  </p>

<h2> 13. Changes to Our Privacy Policy </h2>
<p> Eduloans may modify or update this Privacy Policy from time to time to reflect changes in our business and practices, and so you should review this page periodically (www.Eduloans.us/privacy). If we make changes to this Privacy Policy, we will revise the “Last Updated” date at the top of this policy. If we make a material change to this Privacy Policy, we will notify you as required by law. Your continued use of the Service following any changes signifies your acceptance of our Privacy Policy as modified. </p>

California Consumer Privacy Act Notice for California Consumers
GLBA Notice
PLEASE PRINT AND RETAIN A COPY OF THIS PRIVACY POLICY FOR YOUR RECORDS.
California Consumer Privacy Act Notice for California Consumers

<p> This California Consumer Privacy Act Notice for California Consumers (“CCPA Notice”) supplements BITC Corporation, and its subsidiary, BITC Corporation, India, (collectively, “Company” or “Eduloans”) Privacy Policy and Gramm-Leach-Bliley Act (“GLBA”) notice. This notice applies solely to California residents. The California Consumer Privacy Act (“CCPA”) affords California consumers certain rights over their personal information (subject to certain exceptions). This CCPA Notice outlines those rights and explains how such rights may be exercised. This CCPA Notice also summarizes our data collection and sharing practices, as required by the CCPA. Terms used but not defined shall have the meaning ascribed to them in the CCPA. For the purpose of this CCPA Notice, we refer to our websites, applications that run on mobile devices and tablets, call centers, and other products and services as “Services” or “Sites.” </p>

<p> <strong> 1. What Information is Covered by this CCPA Notice? </strong>
This CCPA Notice relates to the Company’s collection, use and disclosure of California consumers’ personal information. “Personal information” generally means information that is reasonably capable of being associated with you or your household. For a specific list of the categories of personal information we collect, please see Section 5 (Summary of Collection, Use and Disclosure of Personal Information) below. </p>

<p> <strong> 2. Where Do We Collect Personal Information From? </strong>
We collect personal information with prior consent of the user where in while eliciting enquiries he provides the necessary information which is forwarded to third party Financial Institutions. </p>

<p> <strong> 3. How Do We Use Personal Information? </strong>
We may use your personal information for a variety of “business purposes” and “commercial purposes,” as defined by the CCPA. “Business purpose” means the use of personal information for an operational or other notified purpose including, for example, performing services on behalf of another organization. “Commercial purpose” refers to when we use personal information for commercial or economic interests including, for example, by marketing our Services. </p>

<p> <strong> 4. Who Do We Share Personal Information With? </strong>

<p> We may share your personal information with the third parties described below. </p>
<p> <strong> Providers </strong> We may share your personal information with third parties we give you opportunities to interact with through the use of our Services such as when we provide you with the opportunity to obtain prequalified rates from lenders for a education loan or to submit your loan application to a lender. The term “Provider” referenced here is defined in our Terms of Service. Remember that any information you provide to the Provider, whether through us or on your own, will be subject to their privacy practices and terms and conditions. </p>

<p> <strong> Service Providers </strong> We may share your personal information with third parties who work on behalf of, or with, us such as vendors, processors, agents and other representatives (collectively, “Service Providers”). Service Providers assist us with a variety of functions. This includes, for example, assisting with customer service-related functions, providing advertising (including interest-based advertising) and analytics services (e.g., Google Analytics and Facebook Custom Audiences), helping us verify your identity, obtaining your credit report, providing website hosting services (e.g., Amazon Web Services), assisting with auditing functions, and helping us with marketing and related research activities. </p>

<p> <strong> Marketing Partners </strong> We may share your personal information with third-party marketing partners including partners who we conduct joint-marketing activities with or who we offer a co-branded service with (collectively, “Marketing Partners”). Marketing Partners also include those entities who maintain a link to our Services on their site, or a Eduloans widget on their site, and you interact with that widget or click from their site to our Services. </p>

<p> <strong> Co-Signers </strong> As a prospective borrower or applicant of a product or service, your personal information may be shared with a co-signer or co-applicant that you designate (“Co-Signers”). </p>

<p> <strong> Public Forums </strong> In certain circumstances, we may provide opportunities for you to publicly post reviews, questions, comments, suggestions or other content, which may include Personal Information (like your name and email address). This information, of course, may be visible to the public and posted on our Sites. For the purpose of this CCPA Notice, third parties who are able to view and access personal information in accordance with this section shall be referred to as the “General Public.” </p>

<p> <strong> Affiliate Companies </strong> We may share personal information with our parent company and wholly owned subsidiaries (collectively, “Affiliate Companies”). </p>

<p> <strong> Special Circumstances </strong> We may disclose your personal information to third parties: (i) where we have a good faith belief that such disclosure is necessary to meet any applicable law, regulation, legal process or other legal obligation; (ii) to protect or prevent harm or financial loss; (iii) to protect our legitimate interests and legal rights; and (iv) to detect, investigate and help prevent security, fraud or technical issues (collectively, “Special Circumstances”). For the purpose of this CCPA Notice, third parties who receive personal information in accordance with this section shall be referred to as “Special Circumstances Recipients.” </p>

<p>Corporate Transactions We may transfer your personal information in the event we sell or transfer, or are considering selling or transferring, all or a portion of our business or assets (collectively, “Corporate Transactions”). For the purpose of this CCPA Notice, third parties who receive personal information in accordance with this section shall be referred to as “Corporate Transaction Recipients.” </p>

<p> <strong> 5. Summary of Collection, Use, and Disclosure of Personal Information. </strong>
The table below summarizes our collection and sharing practices relating to personal information, including with respect to personal information we have collected, used and disclosed in the preceding 12 months. As shown below, we may have disclosed or sold your personal information to third parties for a business or commercial purpose in the preceding 12 months. For all of the categories of personal information identified below, we may use your personal information for disclosures relating to Corporate Transactions and Special Circumstances. As such, all personal information may be shared with Corporate Transaction Recipients and Special Circumstances Recipients. 
<p>



<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <!-- <thead>
            <tr>
                <th>#</th>
                <th>Table  </th>
                <th>Table  </th>
                <th>Table  </th>
                <th>Table  </th>
                <th>Table  </th>
                <th>Table  </th>
            </tr>
        </thead> -->
        <tbody>


     <tr>
                <td>Identifiers such as name, Social Security number, address, telephone number, </td>
                <td>Information collected directly from you   </td>
                <td>Provide, develop, maintain and improve our Services  </td>
                <td>Providers, Service Providers </td>
                <td>Yes  </td>
                <td>Yes  </td>
                
            </tr>

                 <tr>
               
                <td>Driver’s license or state identification card number, bank account number, loan identifier, credit card information, unique personal identifier, online identifier, Internet Protocol address, email address, account name, or any other similar identifiers  </td>
                <td>Information collected directly from you (including online and offline), Providers, Service Providers, Affiliate Companies, Information automatically derived from use of our Services (e.g., through cookies)  </td>
                <td>Provide, develop, maintain and improve our Services, Process and fulfill a request or other transaction submitted to us,
Administering our Best Rate Guarantee program, Communicate with you (e.g., to schedule meetings with you or to answer questions relating to our Services) Provide personalized services, including content, ads, products or services, To conduct surveys and offer sweepstakes, drawings, and similar promotions, Marketing and advertising our Services and the services of other third parties, Research and analytical purposes (e.g., identifying trends and effectiveness of marketing campaigns), For our business operations (e.g., auditing transactions and performing quality control) </td>
                <td>Co-Signers, Affiliate Companies </td>
                <td>   </td>
                <td>   </td>
            </tr>

            <tr>
                
                <td>Characteristics of protected classifications under California or federal law (e.g., race, religion, and gender)  </td>
                <td>Information collected directly from you (including online and offline), Affiliate Companies</td>
                <td>Process and fulfill a request or other transaction submitted to us, Provide personalized services, including content, ads, products or services</td>
                <td>Providers  </td>
                <td>Yes  </td>
                <td>No  </td>
            </tr>

            <tr>               
                <td>Records of personal property, property-related details, existing loan information (e.g., lender name, estimated balance, type of loan), assets owned, sources of assets)  </td>
                <td>Information collected directly from you (including online and offline), Service Providers, Affiliate Companies,        Publicly accessible sources  </td>
                <td>Process and fulfill a request or other transaction submitted to us, Provide personalized services, including content, ads, products or services, Research and analytical purposes (e.g., identifying   </td>
                <td>Providers, Service Providers, Co-Signers, Affiliate Companies  </td>
                <td>Yes  </td>
                <td>Yes  </td>
            </tr>


               <tr>
                <td> </td>
                <td> Organizations that we enter into contractual agreements with to license/purchase personal information  </td>
                <td>trends and effectiveness of marketing campaigns), For our business operations (e.g., auditing transactions and performing quality control)  </td>
                <td>   </td>
                <td>   </td>
                <td>   </td>
             
            </tr>


               <tr>
                <td>Internet or other electronic network activity information, including browsing history, search history, and information regarding a consumer’s interaction with an internet website, application, or advertisement</td>
                <td>Information collected directly from you (including online and offline), Information automatically derived from use of our Services (e.g., through cookies), Affiliate Companies, Service Providers </td>
                <td>Provide, develop, maintain and improve our Services. Provide personalized services, including content, ads, products or services. Marketing and advertising our Services and the services of other third parties. Research and analytical purposes (e.g., identifying trends and effectiveness of marketing campaigns). </td>
                <td>Providers, Service Providers, Marketing Partners, Affiliate Companies </td>
                <td>Yes  </td>
                <td>Yes  </td>
               
            </tr>


               <tr>
                <td>Professional or employment-related information (e.g., employment history) or education information</td>
                <td>Information collected directly from you (including online and offline), Affiliate Companies</td>
                <td>Process and fulfill a request or other transaction submitted to us. rovide personalized services, including content, ads products and services  </td>
                <td>Providers Affiliate Companies  </td>
                <td>Yes  </td>
                <td>Yes  </td>
         
            </tr>




            <tr>
                <td>Information you provide in reviews, questions, and comments</td>
                <td>Information collected directly from you (including online and offline)   </td>
                <td>Provide, develop, maintain and improve our Services, Marketing and advertising our Services and the services of other third parties / For our business operations (e.g., auditing transactions and performing quality control)</td>
                <td>General Public, Providers, Affiliate Companies </td>
                <td>Yes  </td>
                <td>Yes  </td>             
            </tr>

        </tbody>
    </table>
</div>


 
























<p> <strong> 6. Children </strong>
Our Site and Services are not intended for minors under 16 years of age. As such, we do not knowingly collect or maintain any data from minors under 16 years of age and, as such, do not sell personal information of minors under 16 years of age. </p>

<p> <strong> 7. Your CCPA Rights </strong>
Subject to certain exceptions, the CCPA affords you the following rights with respect to your personal information. It’s worth noting that these rights do not apply to personal information we collect about California consumers who apply for or obtain a financial product and service for personal, family, or household purposes (in other words, information regulated by the Gramm-Leach-Bliley Act). </p>
<p>  <strong>  Request to Know</strong> You have a right to request that the Company disclose what personal information it collects, uses, discloses, and sells. It includes a request for any or all of the following: </p>

<ul>
<li> Specific pieces of personal information that the Company has about you. </li>
<li> Categories of sources from which your personal information is collected. </li>
<li> Categories of personal information the Company has collected about you. </li>
<li> Categories of personal information that the Company has sold or disclosed for a business purpose about you. </li>
<li> Categories of third parties to whom your personal information was sold or disclosed for a business purpose, and  The business or commercial purpose for collecting or selling personal information.</li>
</ul>

<p> <strong> Request to Delete</strong> You have a right to request that the Company delete your personal information that we have collected from you. However, certain information regulated by other privacy and data protection laws (including, for example, the Gramm-Leach-Bliley Act) may be exempt from, or outside the scope of, any deletion request (i.e., the GLBA’s requirement to retain the information may preempt your request that we delete it. In other words, we may be legally required to retain your data for a certain period of time and therefore we cannot delete your data at your request. if this happens, we will let you know in response to your request). As a result, in some instances, we may decline all or part of a deletion request if related to this type of exempt information.</p>

<p> <strong> Request to Opt-Out</strong> We do not provide personal information about you to other companies for money. The advertising services we use may collect certain information, including cookies and similar information stored on your browsers, advertising identifiers on your mobile devices, and/or the IP address of your devices when you visit our sites or use our apps. Those advertising services we use may tailor and deliver ads to you on your devices. Even though we do not provide personal information to these companies for money, California law may characterize this activity by the advertising providers as “sales” of personal information. You have a right to request that these companies discontinue these targeted advertising activities. </P>

<p> <strong> No Discrimination</strong>  You have a right not to receive discriminatory treatment by the Company for the exercise of the privacy rights conferred by the CCPA.In case you would like to exercise your CCPA rights we would request you to email us at support@eduloans.org.</p> 





















</div>

</div>

</div>

</div>

 

<?php include "footer.html" ?>
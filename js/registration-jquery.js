// jQuery().ready(function() {

  $(document).ready(function () {


    // validate form on keyup and submit

    var v = jQuery("#basicform").validate({

      rules: {

        name: {
          required: true,
          minlength: 2,
          maxlength: 16
        },

        email: {

          required: true,
          minlength: 2,
          email: true,
          maxlength: 100,

        },

        // date:{
        //   required: true,
        //   dateISO: true
        // },


        phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
        },

        yrs_address:{

          required: true,
          maxlength: 4,
          digits: true,
        },

        address: {
          required: true,
          maxlength: 200,
		},
		  school: {
          required: true,
        },

		Course:{
          required: true,
        	},

		 Valueupload:{
          required: true,
		 },

		  durationprogram:{
		  required: true,
	  		},

		  Financial:{
          required: true,
        	},

		   financialaid:{
          required: true,
        	},

		  Co_name: {
			  required: true,
		  },

		  Co_email: {

          required: true,
          minlength: 2,
          email: true,
          maxlength: 100,

        },

//         Co_date:{
//           required: true,
//           dateISO: true
//         },


        Co_phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
			},


		 Co_home_phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
			},


		  Co_work_phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
			},

		   Co_yrs_address:{

          required: true,
          maxlength: 4,
          digits: true,
        },

		  Co_social_security: {
			  	required: true,
		  },

		  Co_green_card: {
			  required:true,
		  },

		  Co_relationship:{
			  required:true,
		  },

		  Co_year_living:{
				required:true,
		  },

		  Co_Graduation_Degree: {
		    required:true,
		  },

		  Co_passing_year: {
			  required:true,
		  },

		  Co_Years_achieving: {
			  required:true,
		  },

		  Co_Permanent_address: {

			  required:true,
			  maxlength: 200,
		  },

		  Name_of_Employer:{
			   required: true,
		  },

		  Employer_Contact_No: {
			   required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,

		  },

		  Length_of_Employment:{
			   required: true,
		  },

		  Co_Occupation: {
			  required:true,
		  },

		  Co_gross_income: {
			  required:true,
		  },

		  Oc_other_income: {
			  required:true,
		  },

		  address_Employer: {
		    required: true,
		  },

		  Co_Mortgage: {
			  required:true,
		  },
		  Oc_Outstanding: {
		   required:true,
	  },
		  Oc_Monthly_Property_tax: {
			    required:true,
	  },
		  Oc_Monthly_Home_Owner: {

		  required:true,
	  },

		  Oc_Name_mortgage: {
			   required:true,
		  },

		  Permanent_address_usa: {

			  required:true,
			  maxlength: 200,
		  },
		  usa_name: {
			  required:true,
		  },


		  usa_email: {

          required: true,
          minlength: 2,
          email: true,
          maxlength: 100,

        },


        usa_phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
			},


		 usa_home_phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
			},


		  usa_work_phone: {
          required: true,
          minlength: 10,
          // maxlength: 10,
          // number: true,
          digits: true,
			},

		   usa_yrs_address:{

          required: true,
          maxlength: 4,
          digits: true,
        },

		  usa_yrs_achieving:
		  {
			  required: true,
          maxlength: 4,
          digits: true,
        },
		  usa_Permanent_address : {

			     required: true,

		  },


        // upass1: {
        //   required: true,
        //   minlength: 6,
        //   maxlength: 15,
        // },

        // upass2: {
        //   required: true,
        //   minlength: 6,
        //   equalTo: "#upass1",
        // }



      },

      errorElement: "span",

      errorClass: "help-inline-error",

    });



    $(".open2").click(function() {

      if (v.form()) {

        var utm_source_value = $.cookie('utm_source');

        $.ajax({
              url: 'https://api.eduloans.org/partner/auth/data',
              headers: {
                  'source':'5'
              },
              dataType: 'json',
              type: 'post',
              contentType: 'application/json',
              data: JSON.stringify( {
                 "student_name": $("#name").val(),
                 "student_email": $("#email").val(),
                 "student_number": $("#phone").val(),
                 "type_of_loan": $("#type_of_loan").val(),
                 "loan_amount": $("#loan_amount").val(),
                 "student_state": $("#usState").val(),
                 "residence_year": $("#yrs_address").val(),
                 "student_address": $("#address").val(),
                 "utm_source" : utm_source_value,
                 "hear_about_us_id": 414
              }),
              success: function(data){

                console.log('succes: ', data.data.access_token, data);

                $.cookie('access_token', data.data.access_token);
                $.cookie('lead_id', data.data.lead_id);
                $.cookie('customer_id', data.data.customer_id);
                $.cookie('type_of_loan', $("#type_of_loan").val());

                $(".frm").fadeOut("fast");

                $("#sf2").fadeIn("slow");
                $(".forstep1").removeClass("active");
                $(".forstep2").addClass("active");

              }
              ,
              error: function (error) {
                console.log('error: ', error);
                alert(error.responseJSON.message);
              }

            });

      }

    });



    $(".open3").click(function() {

      if (v.form()) {

        $(".frm").fadeOut("fast");

        $("#sf3").fadeIn("slow");
        $(".forstep2").removeClass("active");
        $(".forstep3").addClass("active");
      }

    });


    $(".open4").click(function() {

      if (v.form()) {

        $(".frm").fadeOut("fast");

        $("#sf4").fadeIn("slow");
        $(".forstep3").removeClass("active");
        $(".forstep4").addClass("active");
      }

    });

    $(".open5").click(function() {

      if (v.form()) {

        $(".frm").fadeOut("fast");

        $("#sf5").fadeIn("slow");
        $(".forstep4").removeClass("active");
        $(".forstep5").addClass("active");
      }

    });

    $(".open6").click(function() {

      if (v.form()) {

        $(".frm").fadeOut("fast");

        $("#sf6").fadeIn("slow");
        $(".forstep5").removeClass("active");
        $(".forstep6").addClass("active");
      }

    });




    $(".submit_data").click(function() {

      if (v.form()) {

        var type_of_loan = $.cookie('type_of_loan');
        var lead_id = $.cookie('lead_id');
        var access_token = $.cookie('access_token');

        $.ajax({
              url: 'https://api.eduloans.org/partner/opteduniversities/data',
              headers: {
                  'source':'5',
                  'access-token' : access_token
              },
              dataType: 'json',
              type: 'post',
              contentType: 'application/json',
              data: JSON.stringify({
                 "university_id" : $("#usUniversities").val(),
                 "course_id" : $("#usCourses").val(),
                 "type_of_loan" : type_of_loan,
                 "total_cost_of_study" : $("#total_cost_of_study").val(),
                 "durantion_of_program" : $("#durationprogram").val(),
                 "lead_id" : lead_id
              }),
              success: function(data){

                console.log('succes: ', data);


                $("#loader").fadeIn();

                 setTimeout(function(){

                   $("#basicform").html('<h2>Thanks for your time.</h2>');
                   $(".offers").addClass("active");

                 }, 1000);

                return false;

              }
              ,
              error: function (error) {
                console.log('error: ', error);
                alert(error.responseJSON.message);
              }

            });


      }

    });



// back button code


    $(".back1").click(function() {

      $(".frm").fadeOut("fast");

      $("#sf1").fadeIn("slow");
        $(".form_steps_tabs_slider li").removeClass("active");
        $(".forstep1").addClass("active");

    });



    $(".back2").click(function() {

      $(".frm").fadeOut("fast");

      $("#sf2").fadeIn("slow");
      $(".form_steps_tabs_slider li").removeClass("active");
        $(".forstep2").addClass("active");
    });

    $(".back3").click(function() {

      $(".frm").fadeOut("fast");

      $("#sf3").fadeIn("slow");
      $(".form_steps_tabs_slider li").removeClass("active");
        $(".forstep3").addClass("active");

    });

    $(".back4").click(function() {

      $(".frm").fadeOut("fast");

      $("#sf4").fadeIn("slow");
      $(".form_steps_tabs_slider li").removeClass("active");
        $(".forstep4").addClass("active");

    });

    $(".back5").click(function() {

      $(".frm").fadeOut("fast");

      $("#sf5").fadeIn("slow");

      $(".form_steps_tabs_slider li").removeClass("active");
        $(".forstep5").addClass("active");

    });

    $(".back6").click(function() {

      $(".frm").fadeOut("fast");

      $("#sf6").fadeIn("slow");
      $(".form_steps_tabs_slider li").removeClass("active");
        $(".forstep6").addClass("active");

    });


    $.ajax({
            url: 'https://api.eduloans.org/partner/usstate/list',
            type: 'GET',
            dataType: 'json',
            headers: {
                'source': '5'
            },
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
               // CallBack(result);
               console.log(result);
               var options =  '<option value=""><strong>Select State</strong></option>';
               $(result.data).each(function(index, value){ //loop through your elements

            options += '<option value="'+value.id+'">'+value.display_name+'</option>'; //add the option element as a string
                });

            $('#usState').html(options);

            },
            error: function (error) {

            }
        });

        //university list

        $.ajax({
                url: 'https://api.eduloans.org/partner/usuniversity/list',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'source': '5'
                },
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                   // CallBack(result);
                   console.log(result);
                   var options11 =  '<option value=""><strong>Select University</strong></option>';
                   $(result.data).each(function(index, value){ //loop through your elements

                options11 += '<option value="'+value.id+'">'+value.display_name+'</option>'; //add the option element as a string
                    });

                $('#usUniversities').html(options11);

                },
                error: function (error) {

                }
            });

        //courses list

        $.ajax({
                url: 'https://api.eduloans.org/partner/uscourses/list',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'source': '5'
                },
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                   // CallBack(result);
                   console.log(result);
                   var options12 =  '<option value=""><strong> Select Course </strong></option>';
                   $(result.data).each(function(index, value){ //loop through your elements

                options12 += '<option value="'+value.id+'">'+value.display_name+'</option>'; //add the option element as a string
                    });

                $('#usCourses').html(options12);

                },
                error: function (error) {

                }
            });

  });

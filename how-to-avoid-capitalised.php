<?php include "header.html" ?>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="blog-page-intro blog-img-1">

                        </div>

                </div>

        </div>

        <div class="row">

                <h1 class="text-center">How to avoid Capitalised Student Interest</h1>

                <div class="col-md-8">

                        <div class="inner-main-content-holder">

                                <p> Interest is one of the most commonly misunderstood aspects of student loan repayment- and the concept of capitalized interest can be especially confusing. But you could potentially save yourself thousands of dollars over the repayment period of your student loans by knowing what capitalized interest is and how to avoid it. </p>

                                <h5>What is Capitalized Interest?</h5>

                                <p>Capitalized interest refers to a process where the unpaid or outstanding interest on your student loan is added to your total loan balance and as a result, you pay interest on that new amount. For example, while you’re in school, your lender may not require you to start making payments on your loans. You may have a grace period that extends until after you graduate, for example. Still, interest likely accrues while you’re in school—and if you don’t pay off that interest as you go, you may end up with a large amount of accrued interest by the time your grace period ends. At that time, your interest could capitalize and be added to your total loan balance. From there, you’re paying interest not only on the original amount you borrowed, but on the capitalized amount that was added to your balance.</p>

                                <h5>What Causes Interest to Capitalize?</h5>

                                <p> Interest can only capitalize under certain conditions. For starters, you need to have an outstanding interest balance on your account. Some loan types, such as subsidized federal loans, do not accrue interest until after you have graduated or after your grace period has ended since subsidized loans don’t accrue interest. </p>

                                <p> Many loans, however, are unsubsidized and do begin accruing interest as soon as they are disbursed. This includes both private and federal loans. You’ll need to check the terms of your borrowing agreement to determine how/when interest may capitalize. However, you can generally expect unpaid student loan interest to capitalize if you don’t make your payments when:</p>

                                <ul>

 <li>Your grace period on the loan has ended. This means you’re required to begin making payments on your loan balance.</li>

 <li>Your deferment or forbearance period has ended. If you’ve had your loans deferred to postpone payments, interest may still have accrued even when you weren’t making payments.</li>

 <li>You no longer qualify for your income-based repayment plan. If you’ve been making payments on an income-based repayment plan, you’re required to prove eligibility each year. If you fail to file the proper paperwork or otherwise become ineligible for your repayment plan, you will move into full repayment.</li>
 

                                </ul>

                                 
<h5> A Realistic Example </h5>
<p>To better understand capitalized interest, it can be helpful to look at an example.</p>
<p>Consider a scenario where you took out a $10,000 unsubsidized loan to pay for your college tuition. You didn’t make any interest payments while you were in school, so the loan accrued $3,000 in interest by the time you graduated. Once your grace period on the loan ended and you still had $3,000 of outstanding interest, your lender capitalized the interest by adding it to your total balance. Now, you have a loan balance of $13,000—and you continue to accrue interest on all of it.</p>

<p>Now, not only do you need to pay off the initial $10,000 you borrowed, but you’re also paying back the $3,000 in interest plus whatever additional interest accrues on that balance. This can easily add up to thousands of extra dollars you’re paying over the life of your loan all because your interest capitalized.</p>

<h5>How to Avoid Capitalized Interest</h5>

<p>As you can see, it isn’t ideal to have student loan interest capitalize, as it can cost you a lot of money on top of what you’re already responsible for. The good news is that there are plenty of ways to go about avoiding capitalized interest altogether.</p>
<h5>Take Advantage of Subsidized Loans</h5>

<p>One of the best ways to avoid interest capitalization altogether is to take out subsidized loans to pay for your schooling. Of course, this is easier said than done when you consider that there are borrowing limits on subsidized loans. For example, there is currently a $23,000 cap on federal subsidized loans for undergraduates and a $65,500 cap on subsidized graduate student loans, which may not be enough to cover all of your borrowing needs. Subsidized loans are also only available to those with exceptional financial need, so not everybody will qualify.</p>

<p>Still, if you’re planning on taking out loans for school, be sure to exhaust your federal subsidized borrowing options so you can avoid accruing interest while you’re in school. Just keep in mind that you will accrue interest once your grace period ends, so you could still face capitalization if you fail to make payments.</p>

<h5>Pay Off Interest While You’re in School</h5>

<p>If you have to take out any unsubsidized loans while you’re in school, the key to avoiding interest capitalization is to simply pay off your student loan interest on a monthly basis. This doesn’t mean you have to make full monthly payments on your student loan balance; simply pay enough each month to stay on top of your interest payments. Some students may also prefer to wait until they receive their tax refunds so they can use the money to pay off their accrued interest for the year in-full. </p>

<p>Either way, paying down your interest while you’re in school will help you avoid capitalization and possibly save you hundreds or thousands of dollars over the life of your loan. LendKey is happy to offer an option where borrowers can pay $25 per month while in school to go towards interest or pay full interest balance each month.</p>

<h5>Make Interest Payments During Forbearance</h5>

<p>If you’re already in repayment and are having trouble making your student loan payments, you might have applied for forbearance as a means of putting off your payments until you’re more financially stable. This can be a way to lessen some of your financial burdens during a difficult time, such as a period of unemployment.</p>

<p> Keep in mind, however, that interest does continue to accrue during most forbearance periods. This means that even though you may not owe anything on your student loans (just like when you were in school), your interest is still building up. In most cases, unpaid interest will capitalize when your forbearance period ends—so it’s in your best interest to stay on top of your interest payments during forbearance the best you can.</p>

<h5>What if You Can’t Make Your Payments? </h5>

<p>If you can’t afford to make your student loan payments, there are options available to you to avoid default. Always begin by contacting your lender to look into adjusting your repayment plan. Most lenders offer a wide range of repayment plan options to suit your needs. Typically, you will be automatically enrolled in a standard repayment plan, but other arrangements may be available. The federal government offers these other plans, like Income-Driven repayment, but private lenders may not.</p>

<p>As mentioned above, deferment or forbearance may also be an option for putting off your student loans. Regardless of which route you take to avoid default, it’s important that you always read the fine print and make sure you understand how your interest may be affected or when it may be capitalized. </p>

<h5>Paying Off Your Student Loan Debt Efficiently</h5>

<p> With a little research and planning, you can pay off your student loan debt as quickly and efficiently as possible. Whether you’re getting ready to apply for student loans or will soon be entering your repayment period, there are some practical tips worth keeping in mind.<p>

<h5>Look Into Consolidation or Refinancing</h5>

<p>If you have multiple loans with varying interest rates, or if your financial situation has changed since you took out your loans, it may be worth looking into consolidation or refinancing. Student loan refinancing may allow you to secure a lower interest rate on your current loans while also consolidating multiple loans into one single balance. This can save you a lot of money over the repayment period of your loan.</p>

<h5>Get Help From Your Employer</h5>

<p>Some employers will offer reimbursement for tuition or may even contribute to your student loan payments. If you’re a recent graduate looking for your first job in the field, consider researching employers that offer student loan payment contributions.</p>

<h5>Pay a Little Extra When You Can</h5>

<p>Whenever possible, throw a little extra money towards your student loan repayment. This doesn’t mean you have to allocate an extra $100 to your student loans every single month. However, if you find yourself with some extra cash, putting it towards your student loans can go a long way. Even making the equivalent of one extra loan payment each year could save you a lot of money in interest and potentially allow you to pay off your balance sooner.</p>

<h5>The Bottom Line </h5>

<p>Student loan interest rates can be a little confusing—but one of the most important things you can remember is to avoid letting your interest capitalize. By staying on top of your interest payments during school and throughout your grace period, and by taking advantage of subsidized loan options, you can save yourself a nice chunk of change later on. Combine this with making wise decisions about repaying your loans quickly and you can avoid a lot of financial stress later on.</p>

 
                        </div>

                </div>

                <aside class="col-md-4">

                        <div class="sidebar-content sticky-sidebar">

                                <div class="sticky-side-menu">

                                        <h4>Blogs</h4>

                                        <ul>

                                                <a href="javascript:void(0);"><li>Paying for Your College</li></a>

                                                <a href="how-to-save-money-by-refinancing.php"><li>How To Save Money By Refinancing?</li></a>

                                        </ul>

                                </div>

                                <div class="special-offer">

                                        <img src="images/offer-1.png" alt="Offer" />

                                        <!-- <h4>Get 10% Off</h4> -->

                                        <a href="student-registration.php"><button type="button" class="btn-apply-inner">Apply Now</button></a>

                                </div>

                        </div>

                </aside>

        </div>

</div>





























<?php include "footer.html" ?>
<?php include "header.html" ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-intro">
				<p class="my-breadcrumbs">Home / 404</p>
				<h1>404</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem iusto praesentium tempore, iste nemo a, non perferendis molestiae, architecto distinctio eligendi qui nulla fugiat, alias excepturi deserunt iure. Dolorem, quas!</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="inner-main-content-holder">
				<section class="no-page-block container text-center">
					<div class="row">
						<div class="col-xs-12 col-sm-offset-2 col-sm-8">
							<h1>Page Not Found!</h1>
							<p>Sorry, We couldn't find the page you're looking for. <br class="hidden-xs">Try returning to the Homepage</p>
							<strong class="element-block text-large">4<span class="text-bright">0</span>4</strong>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<?php include "footer.html" ?>
<?php include "header.html" ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-intro">
				<p class="my-breadcrumbs">Home / Co-Signer Registration</p>
				<h1>Co-Signer Registration</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque eius expedita libero, voluptatum saepe. Tempora, nam temporibus totam maxime odit quasi eos voluptatem atque corporis cupiditate adipisci, itaque ducimus quam.</p>
			</div>
		</div>
	</div>
	<div class="row my-shadow-effect">
		<form action="" class="contact-form">
			<div class="col-xs-12 col-sm-12 col-md-12">
				
				<h2 class="text-center my-section-header">Personal Information</h2>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block" placeholder="Name">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block" id="date" name="date" placeholder="DOB (dd/mm/yyyy)">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="email" class="form-control element-block" placeholder="Email">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Mobile Phone No.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Home Phone no.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Work Phone No.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="No. of years at the address">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Social Security No.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Green Card/Citizen">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Relationship with Borrower">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Year of living in USA">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Graduation Degree">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Passing year">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Years of achieving green card/Citizenship">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						
						<div id="file-upload-form">
							<input id="file-upload" type="file" name="fileUpload" />
							<label for="file-upload" id="file-drag">
								Select a Passport to upload
								<br />OR
								<br />Drag a Passport into this box
								
								<br /><br /><span id="file-upload-btn" class="button">Add Passport</span>
							</label>
							
							<!-- <progress id="file-progress" value="0">
										<span>0</span>%
							</progress> -->
							
							<output for="file-upload" id="messages"></output>
						</div>
						<?php
						//$fn = (isset($_SERVER['HTTP_X_FILE_NAME']) ? $_SERVER['HTTP_X_FILE_NAME'] : false);
						//$targetDir = 'tmp/';
						//if ($fn) {
							//if (isFileValid($fn)) {
								// AJAX call
								//file_put_contents(
									//$targetDir . $fn,
									//file_get_contents('php://input')
								//);
								//removeFile($fn);
							//}
						//}
						//function removeFile($file) {
							//unlink($targetDir . $file);
						//}
						?>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div id="file-upload-form">
							<input id="file-upload" type="file" name="fileUpload" />
							<label for="file-upload" id="file-drag">
								Select a Driving License to upload
								<br />OR
								<br />Drag a Driving License into this box
								
								<br /><br /><span id="file-upload-btn" class="button">Add Driving License</span>
							</label>
							
							<!-- <progress id="file-progress" value="0">
										<span>0</span>%
							</progress> -->
							
							<output for="file-upload" id="messages"></output>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<textarea class="form-control element-block" placeholder="Permanent address in USA"></textarea>
						</div>
					</div>
					
				</div>
				
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				
				<h2 class="text-center my-section-header">Employer Information</h2>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block" placeholder="Name of Employer">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Employer Contact No.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Length of Employment">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6">
						<div id="file-upload-form">
							<input id="file-upload" type="file" name="fileUpload" />
							<label for="file-upload" id="file-drag">
								Select a Appointment Letter to upload
								<br />OR
								<br />Drag a Appointment Letter into this box
								
								<br /><br /><span id="file-upload-btn" class="button">Add Appointment Letter</span>
							</label>
							
							<!-- <progress id="file-progress" value="0">
										<span>0</span>%
							</progress> -->
							
							<output for="file-upload" id="messages"></output>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						
						<div id="file-upload-form">
							<input id="file-upload" type="file" name="fileUpload" />
							<label for="file-upload" id="file-drag">
								Select a Last six months’ pay slips/Pay stubs to upload
								<br />OR
								<br />Drag a Last six months’ pay slips/Pay stubs into this box
								
								<br /><br /><span id="file-upload-btn" class="button">Add Pay Slips/Pay Stubs</span>
							</label>
							
							<!-- <progress id="file-progress" value="0">
										<span>0</span>%
							</progress> -->
							
							<output for="file-upload" id="messages"></output>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Occupation:  - Engineer / Nurse etc">
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Gross Income from Employment ">
						</div>
					</div>
					<div class="col-xs-12 col-sm-2">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Other Income">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<textarea class="form-control element-block" placeholder="Address of Employer"></textarea>
						</div>
					</div>
					
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				
				<h2 class="text-center my-section-header">Apartment Information</h2>
				<div class="row">
					<div class="col-xs-12 col-sm-3">
						<ul class="nav nav-pills nav-stacked">
							
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #777777;">Apartment<span class="caret"></span></a>
								<ul class="dropdown-menu my-dropdown-options">
									<li><a href="#">Rent</a></li>
									<li><a href="#">Fully Owned</a></li>
									<li><a href="#">Mortgage</a></li>
								</ul>
							</li>
							
						</ul>
					</div>
					
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Mortgage payment details">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Outstanding Mortgage Loan">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Monthly Property Tax">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Monthly Home Owner Association Fees">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Name of mortgage lender ">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<textarea class="form-control element-block" placeholder="Permanent address in USA"></textarea>
						</div>
					</div>
					
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				
				<h2 class="text-center my-section-header">Alternate contact in USA</h2>
				<p>2 people one of them has to be a relative (Prefer green card holder/Citizen but H1B would also work)</p>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control element-block" placeholder="Name">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="email" class="form-control element-block" placeholder="Email">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Mobile Phone No.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Home Phone no.">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="tel" class="form-control element-block" placeholder="Work Phone No.">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Year of living in USA">
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control element-block"  placeholder="Years of achieving green card/Citizenship">
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<textarea class="form-control element-block" placeholder="Permanent address in USA"></textarea>
						</div>
					</div>
					
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="text-center">
					<button type="submit" class="btn-submit-contact">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php include "footer.html" ?>
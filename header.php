<?php
?>
<script type="text/javascript">

  $(document).ready(function() {

    var user_access = $.cookie('s_name');

    if(typeof user_access !== 'undefined' && user_access){

      $.ajax({
              url: 'http://dapi.eduloans.org/partner/usstate/list',
              type: 'GET',
              dataType: 'json',
              headers: {
                  'source': '5'
              },
              contentType: 'application/json; charset=utf-8',
              success: function (result) {
                 // CallBack(result);
                 console.log(result);

                 $("#us").hide();

              $('#usState').html(options);

              },
              error: function (error) {

              }
          });

    } else {
      $("#loggedIN").hide();
    }

} );
</script>

<div class="top-bar bg-dark text-gray" id="loggedOUT">
  <div class="container">
    <div class="row top-bar-holder">
      <div class="col-xs-9 col">
        <!-- bar links -->
        <ul class="font-lato list-unstyled bar-links">
          <li>
            <a href="tel:+611234567890">
              <strong class="dt element-block text-capitalize hd-phone">Call :</strong>
              <strong class="dd element-block hd-phone">+(61) 123 456 7890</strong>
              <i class="fas fa-phone-square hd-up-phone hidden-sm hidden-md hidden-lg"><span class="sr-only">phone</span></i>
            </a>
          </li>
          <li>
            <a href="mailto:&#069;&#120;&#097;&#109;&#112;&#108;&#101;&#064;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;">
              <strong class="dt element-block text-capitalize hd-phone">Email :</strong>
              <strong class="dd element-block hd-phone">&#069;&#120;&#097;&#109;&#112;&#108;&#101;&#064;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;</strong>
              <i class="fas fa-envelope-square hd-up-phone hidden-sm hidden-md hidden-lg"><span class="sr-only">email</span></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-xs-3 col justify-end">
        <!-- user links -->
        <ul class="list-unstyled user-links fw-bold font-lato">
          <li><a href="#popup1" class="lightbox">Login</a> <span class="sep">|</span> <a href="#popup2" class="lightbox">Register</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="top-bar bg-dark text-gray hidden" id="loggedIN">
  <div class="container">
    <div class="row top-bar-holder">
      <div class="col-xs-9 col">
        <!-- bar links -->
        <ul class="font-lato list-unstyled bar-links">
          <li>
            <a href="tel:+611234567890">
              <strong class="dt element-block text-capitalize hd-phone">Call :</strong>
              <strong class="dd element-block hd-phone">+(61) 123 456 7890</strong>
              <i class="fas fa-phone-square hd-up-phone hidden-sm hidden-md hidden-lg"><span class="sr-only">phone</span></i>
            </a>
          </li>
          <li>
            <a href="mailto:&#069;&#120;&#097;&#109;&#112;&#108;&#101;&#064;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;">
              <strong class="dt element-block text-capitalize hd-phone">Email :</strong>
              <strong class="dd element-block hd-phone">&#069;&#120;&#097;&#109;&#112;&#108;&#101;&#064;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;</strong>
              <i class="fas fa-envelope-square hd-up-phone hidden-sm hidden-md hidden-lg"><span class="sr-only">email</span></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-xs-3 col justify-end">
        <!-- user links -->
        <ul class="list-unstyled user-links fw-bold font-lato">
          <li><a href="#popup1" class="lightbox">Login</a> <span class="sep">|</span> <a href="registrations.php" class="btn btn-submit-contact" >
            Register
          </a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

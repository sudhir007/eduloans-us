<section class="colored-section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<header class="popular-posts-head">
									<p>From Our Partner Lenders</p>
									<h2 class="popular-head-heading">Compare comprehensive options</h2>
								</header>

								<!-- show on desktop -->
								
								<ul class="nav nav-tabs nav-justified loan-comparision-head">
										<li class="active"><a data-toggle="tab" href="#home">Student Loan Refinancing</a></li>
										<li><a data-toggle="tab" href="#menu1">Private Student Loan</a></li>
										<li><a data-toggle="tab" href="#menu2">Personal Loan</a></li>
								</ul>

								<div class="tab-content loan-comparision-body">
										<div id="home" class="tab-pane fade in active">
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-1.png" alt="loan lender image" class="" />
																<div>Advantage Education Loans</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 4.54%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>10, 15, 20 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-2.png" alt="loan lender image" class="" />
																<div>Brazos</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.10% <strong>Variable:</strong> 2.00%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 7, 10, 15, 20 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-3.png" alt="loan lender image" class="" />
																<div>Citizens Bank</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.45% <strong>Variable:</strong> 2.15%<sup>1</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 7, 10, 15, 20 yrs<sup>1</sup></div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-4.png" alt="loan lender image" class="" />
																<div>College Ave</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.54% <strong>Variable:</strong> 2.62%<sup>1</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5 - 20 yrs<sup>2</sup></div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-5.png" alt="loan lender image" class="" />
																<div>EDvestinU</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong>  4.93% <strong>Variable:</strong> 4.54%<sup>1</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>10, 15, 20 yrs </div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-6.png" alt="loan lender image" class="" />
																<div>ELFI</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.03%<sup>3</sup>  <strong>Variable:</strong> 2.39%<sup>3</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 7, 10, 15, 20 yrs<sup>3</sup> </div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-7.png" alt="loan lender image" class="" />
																<div>MEFA</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.95%<sup>3</sup>  <strong>Variable:</strong> 3.96%<sup>3</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>7, 10, 15 yrs </div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-8.png" alt="loan lender image" class="" />
																<div>PenFed</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.48%   <strong>Variable:</strong> 2.27% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 8, 12, 15 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-9.png" alt="loan lender image" class="" />
																<div>RISLA</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.49% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 10, 15 yrs </div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-10.png" alt="loan lender image" class="" />
																<div>SoFi</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.20%<sup>4</sup> <strong>Variable:</strong> 2.31%<sup>4</sup> </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 7, 10, 15, 20 yrs<sup>4</sup> </div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>

										</div>
										<div id="menu1" class="tab-pane fade">
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/private-student-1.png" alt="loan lender image" class="" />
																<div>Ascent</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 4.21% <strong>Variable:</strong>3.16%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>10, 15, 20 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/private-student-2.png" alt="loan lender image" class="" />
																<div>Citizens Bank	</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 4.40% <sup>5</sup> <strong>Variable:</strong> 2.69%<sup>5</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 10, 15 yrs<sup>5</sup></div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/private-student-3.png" alt="loan lender image" class="" />
																<div>College Ave</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 4.54% <strong>Variable:</strong> 2.84%<sup>1</sup></div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5, 8, 10, and 15 yrs<sup>8</sup></div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>									
											

												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/private-student-4.png" alt="loan lender image" class="" />
																<div>Discover <br> Student Loans</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong>5.09% APR-12.49%APR<sup>12</sup>
																 <strong>Variable:</strong><span>3.15%</span>APR-<span>11.37%</span>APR<sup>12</sup> </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>15, 20 yrs<sup>12</sup></div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>

													<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-5.png" alt="loan lender image" class="" />
																<div>EDvestinU</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 4.52% <strong>Variable:</strong> 3.68% <sup></sup>
 
																</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>7, 10, 12, 15, 20 yrs<sup></sup></div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>


												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/INvestEd.png" alt="loan lender image" class="" />
																<div>INvestEd</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
										          <div><strong>Fixed:</strong> 4.09% <strong>Variable:</strong>3.12%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>5 - 15 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>



											 
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/loan-lender-7.png" alt="loan lender image" class="" />
																<div>MEFA	</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div><strong>Fixed:</strong> 3.95%  </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>10 - 15 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>



	                                             <div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/SallieMae.png" alt="loan lender image" class="" />
																<div>Sallie Mae</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
											 <div> <strong>Fixed:</strong> 4.74%  APR - 11.85% APR<sup>9</sup> <strong> Fixed:</strong> 2.75% APR - 10.65% APR <sup>9</sup> </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>10 - 15 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Eligible degrees</div>
																<div>Undergrad & Graduate</div>
														</div>
												</div>




										</div>
										<div id="menu2" class="tab-pane fade">
												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/Avant.png" alt="loan lender image" class="" />
																<div>Avant</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>9.95% - 35.99%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>2 - 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount </div>
																<div>Up to $35,000</div>
														</div>
												</div>

												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/BestEgg.png" alt="loan lender image" class="" />
																<div>Best Egg</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>5.99% - 29.99% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>3, 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $35,000</div>
														</div>
												</div>

		                                         <div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/FreedomPlus.png" alt="loan lender image" class="" />
																<div>FreedomPlus</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div> 5.99% - 29.99% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>2 - 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $35,000</div>
														</div>
												</div>

                                                  <div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/LendingClub.png" alt="loan lender image" class="" />
																<div>LendingClub</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>6.95% - 35.89% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>3, 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $40,000</div>
														</div>
												</div>


												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/LendingPoint.png" alt="loan lender image" class="" />
																<div>LendingPoint</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>15.49% - 34.99% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>2 - 4 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $25,000</div>
														</div>
												</div>

												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/LightStream.png" alt="loan lender image" class="" />
																<div>LightStream</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>4.99% - 16.79%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>2 - 7 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $100,000</div>
														</div>
												</div>

												<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/Marcus.png" alt="loan lender image" class="" />
																<div>Marcus by Goldman Sachs</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div> 6.99% - 28.99% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>3 - 6 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $40,000</div>
														</div>
												</div>


														<div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/Payoff.png" alt="loan lender image" class="" />
																<div>Payoff</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>5.99% - 24.99% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div> 2 - 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $35,000</div>
														</div>
												        </div>


                                                        <div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/Prosper.png" alt="loan lender image" class="" />
																<div>Prosper</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>6.95% - 35.99% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>3, 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $40,000</div>
														</div>
												        </div>



                                                        <div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/Upgrade.png" alt="loan lender image" class="" />
																<div>Upgrade</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>6.98% - 35.89% </div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>3, 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $50,000</div>
														</div>
												        </div>

												        <div class="loan-lender-container">
														<div class="loan-entry-1">
																<img src="images/Upstart.png" alt="loan lender image" class="" />
																<div>Upstart</div>
														</div>
														<div class="loan-entry-2">
																<div>Rates from (APR)</div>
																<div>6.14% - 35.99%</div>
														</div>
														<div class="loan-entry-3">
																<div>Loan term</div>
																<div>3, 5 yrs</div>
														</div>
														<div class="loan-entry-4">
																<div>Loan amount</div>
																<div>Up to $50,000</div>
														</div>
												        </div>


 

										</div>
								</div>

								<!-- show on mobile -->
								<div class="table-responsive">
									<table class="table">
										<thead style="background: #004b7a;
											color: #fff;">
											<tr>
												<th></th>
												<th>Sallie Mae</th>
												<th>Ascent</th>
												<th>Citizens Bank</th>
												<!-- <th>Common Bond</th>
												<th>Discover</th>
												<th>Earnest</th>
												<th>Lendkey</th>
												<th>Laurel Road</th>
												<th>PNC</th>
												<th>SoFi</th> -->
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Description</td>
												<td>Sallie Mae is a leading bank which has the largest marketshare in Education Loans</td>
												<td>"
													Goal Solutions, Inc. (Goal) and Richland State Bank (RSB) created Ascent Student Loans to help revolutionize the way people pay for college."
												</td>
												<td>Citizesn is the 13th largest retail bank in the United States delivering all financial solutions including Student Loans
												</td>
												<!-- <td>CommonBond is a marketplace lender that refinances graduate and undergraduate student loans for university graduates.
												</td>
												<td>Discover Student Loans are made by Discover Bank, a trusted financial institution for 100 years. Discover Bank offers a variety of financial products, including FDIC-insured savings accounts, credit cards, personal loans and student loans.
												</td>
												<td>Earnest is a Technology company looking to make Credit easy for Students. They work with multiple funds and lending partners.
												</td>
												<td>LendKey connects borrowers with its network of community and regional banks and credit unions. It manages the application, support and servicing of student loans, while the capital for the loan comes from the financial institution.
												</td>
												<td>A division of KeyBank, online lender Laurel Road
												</td>
												<td>PNC was chartered in 1845 and has offered private student loans for more than 50 years.
												</td>
												<td>Social Finance, Inc. is an American online personal finance company that provides student loan refinancing, mortgages and personal loans.
												</td> -->
											</tr>
											<tr>
												<td>Year of Establishment</td>
												<td>1973
												</td>
												<td>2001
												</td>
												<td>1828
												</td>
										<!-- 		<td>2012
												</td>
												<td>2010
												</td>
												<td>2013
												</td>
												<td>2007
												</td>
												<td>2013
												</td>
												<td>1970
												</td>
												<td>2011
												</td>
												 -->
											</tr>
											<tr>
												<td>Total Loan Book Size</td>
												<td></td>
												<td>$26 Billion
												</td>
												<!-- <td></td>
												<td></td>
												<td></td>
												<td>$4.5 billion
												</td>
												<td></td>
												<td></td>
												<td></td>
												<td>$18 billion
												</td>
												 -->
											</tr>
											<tr>
												<td>Loan types</td>
												<td>Undergraduate, Career Training, Parent Loans, K12 Loans, MBA, Medical School, Medical Residency, Dental School, Dental Residency, Law School, Bar Study, Graduate
												</td>
												<td>All Graduate/ Undergraduate studentswith atleast half-time enrolled in a degree program at an eligible institution.
												</td>
												<td>Undergrad, graduate, MBA, law, health care, parent loans
												</td>
												<!-- <td>Undergraduate, graduate, refinancing, MBA, medical
												</td>
												<td>Undergrad, graduate, MBA, law, dental, health care, international, refinancing
												</td>
												<td>Undergraduate, graduate, MBA, law, dental, medical, international, refinancing, parent refinancing
												</td>
												<td>Undergrad, graduate, parent, refinancing
												</td>
												<td>Graduate, residents, fellows, parent, refinance
												</td>
												<td>Undergraduate, graduate, MBA, law, dental, medical, health professions residency, bar study, refinancing
												</td>
												<td>Undergraduate, graduate, MBA, law, dental, medical, parent, refinancing, parent refinancing
												</td> -->
												
											</tr>
											<tr>
												<td>Rate types</td>
												<td>Fixed and Variable
												</td>
												<td>Fixed and Variable
												</td>
												<td>Fixed and variable
												</td>
												<!-- <td>Fixed and variable
												</td>
												<td>Fixed and variable
												</td>
												<td>Fixed and variable
												</td>
												<td>Fixed and variable
												</td>
												<td>Fixed and variable
												</td>
												<td>Fixed and variable
												</td>
												<td>Fixed and variable
												</td> -->
												
											</tr>
											<tr>
												<td>Loan terms</td>
												<td>Five to 15 years
												</td>
												<td>Flexible 5-year, 10-year or 15-year
												</td>
												<td>Five to 15 years
												</td>
												<!-- <td>Five, 10 or 15 years for most loan types
												</td>
												<td>15 to 20 years
												</td>
												<td>Five to 15 years
												</td>
												<td>10 years
												</td>
												<td>Five to 20 years
												</td>
												<td>10 to 15 years
												</td>
												<td>Five to 20 years
												</td> -->
												
											</tr>
											<tr>
												<td>Loan amounts</td>
												<td>$1000  up to the cost of attendance
												</td>
												<td>$2,000 to $200,000
												</td>
												<td>$1,000 to $350,000
												</td>
												<!-- <td>$2,000 up to the cost of attendance
												</td>
												<td>$1,000 to total cost of attendance minus other financial aid
												</td>
												<td>$5,000 to school-certified cost of attendance
												</td>
												<td>$2,000 to total cost of attendance
												</td>
												<td>$5,000 to no max
												</td>
												<td>$1,000 to $75,000 refinance
												</td>
												<td>$5,000 to not disclosed
												</td>
												 -->
											</tr>
											<tr>
												<td>Application or origination fees</td>
												<td>No Application or Origination fees
												</td>
												<td>No origination, disbursement, or loan application fees.
												</td>
												<td>No application fee, origination fee not disclosed
												</td>
												<!-- <td>None for undergraduates with cosigners, an origination fee applies for graduates without a co-signer
												</td>
												<td>No application, origination or late fees
												</td>
												<td>No application fee, origination fee not disclosed
												</td>
												<td>No application fee, origination fee not disclosed
												</td>
												<td>No application fee, origination fee not disclosed
												</td>
												<td>None
												</td>
												<td>No application fee, origination fee not disclosed
												</td> -->
											</tr>
											<tr>
												<td>Discounts</td>
												<td>Autopay
												</td>
												<td>1% cash back, scholarships, a Refer a Friend Program, Autopay
												</td>
												<td>Loyalty, autopay
												</td>
												<!-- <td>Autopay
												</td>
												<td>Autopay, cash reward for at least a 3.0 GPA
												</td>
												<td>Autopay
												</td>
												<td>Autopay
												</td>
												<td>Autopay
												</td>
												<td>Autopay
												</td>
												<td>Autopay, additional SoFi loan discount
												</td> -->
												
											</tr>
											<tr>
												<td>Repayment Options</td>
												<td>In-school interest only, in-school fixed, in-school deferment, immediate repayment
												</td>
												<td>"In-School Interest Only Repayment: Pay
													Deferred Repayment:
													$25 Minimum Payment  Options Available"
												</td>
												<td>Not disclosed
												</td>
												<!-- <td>In-school interest only, in-school fixed, in-school deferment, immediate repayment
												</td>
												<td></td>
												<td>Yes, options available
												</td>
												<td>Enrollment, economic hardship
												</td>
												<td></td>
												<td>Yes, options available
												</td>
												<td>Yes, options available
												</td> -->
												
											</tr>
											<tr>
												<td>Deferment or forbearance hardship options</td>
												<td>Deferment option available
												</td>
												<td>Deferment and Forgiveness available
												</td>
												<td>Economic hardship, military service, post active duty, enrolled or returning to school
												</td>
												<!-- <td>Yes
												</td>
												<td>Deferment or forbearance available
												</td>
												<td>No, refinance only
												</td>
												<td>May be available after 12 to 36 consecutive on-time payments of principal and interest
												</td>
												<td>Up to 12-month forbearance for qualified hardships such as involuntary job loss or unpaid maternity leave
												</td>
												<td>Yes
												</td>
												<td>None specified
												</td> -->
												
											</tr>
											<tr>
												<td>Co-signer release ( Months) </td>
												<td>Can apply for Co-signor release after making 12 payments. Full and on Time
												</td>
												<td>Can apply for cosigner  release after making the 24 consecutive, regularly scheduled full principal and interest payments on-time
												</td>
												<td>Can apply for a co-signer release after 36 consecutive on-time principal and interest payments
												</td>
												<!-- <td>After 24 payments in full and on time
												</td>
												<td>NA
												</td>
												<td>No Cosignor release
												</td>
												<td>No Cosignor release
												</td>
												<td>Can apply to assume full responsibility of the loan after 36 consecutive on-time payments of principal and interest
												</td>
												<td>No Cosignor release
												</td>
												<td>No Cosignor release
												</td>
												 -->
											</tr>
											<tr>
												<td>BBB rating</td>
												<td>A+</td>
												<td>A+</td>
												<td>A+</td>
												<!-- <td>A-
												</td>
												<td>A+
												</td>
												<td>A+
												</td>
												<td>A+
												</td>
												<td>A+
												</td>
												<td>A+
												</td>
												<td>Not Accredited
												</td> -->
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-8 col-sm-8">
								<h4 class="no-worry">Let your education grow, not your worries!</h4>
							</div>
							<div class="col-md-4 col-sm-4">
								<button type="button" class="btn-apply">Apply Now</button>
							</div>
						</div>
					</div>
				</section>
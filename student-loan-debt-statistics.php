<?php include "header.html" ?>

<style type="text/css">

 .lista li{ font-size:15px; padding:2px 0px; list-style-type:square;}   
 .lista li a{ color:#1a44de; }
 .lista li a:hover{ color:#FF9900; }
 .st h3{color:#ff9900; font-size: 18px;} 
 .st strong {color: #463b3b;}
</style>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                 <p class="my-breadcrumbs">Student Loans / Student Loan Debt Statistics</p>  

                                <h1>Student Loan Debt Statistics</h1>

                                <p>It’s no secret that student loans are a hot topic in the United States nowadays. They are mentioned in the news, both in a positive and negative light, almost every day. With all of this news circulating around, it’s natural to become curious about the numbers behind student loan debt. This page takes a look at key student loan statistics and serves as a directory of other pages where you can find more information.</p>

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-8">

                        <div class="inner-main-content-holder st">                    


                                
<h2 id="link-1">Average Student Loan Debt Statistics</h2>

<ul class="lista">
<li><strong>$1.67 trillion</strong>: the total amount of outstanding student loan debt (<a href="https://fred.stlouisfed.org/series/SLOAS" target="_blank" rel="noreferrer noopener" aria-label=" (opens in a new tab)">source</a>)</li>
<li><strong>44.5 million:</strong>&nbsp;the total number of student loan borrowers (<a href="https://www.newyorkfed.org/medialibrary/interactives/householdcredit/data/xls/sl_update_2018.xlsx" target="_blank" rel="noreferrer noopener" aria-label=" (opens in a new tab)">source</a>)</li>
<li><strong>$28,565:</strong>&nbsp;the average student loan debt per borrower from the Class of 2018 (<a href="https://lendedu.com/student-loan-debt-by-school-by-state-2019/">source</a>)</li>
<li><strong>$16,649:</strong>&nbsp;the average student loan debt per graduate from the Class of 2018 (<a href="https://lendedu.com/student-loan-debt-by-school-by-state-2019/">source</a>)</li>
<li><strong>57%:</strong>&nbsp;the percentage of graduates from the Class of 2018 with student debt (<a href="https://lendedu.com/student-loan-debt-by-school-by-state-2019/">source</a>)</li>
<li><strong>10.8%:</strong>&nbsp;the percentage of student debt that is 90+ days delinquent or in default (<a href="https://www.newyorkfed.org/medialibrary/interactives/householdcredit/data/pdf/HHDC_2020Q1.pdf" target="_blank" rel="noreferrer noopener">source</a>)</li>
<li><strong>$200-$299:</strong>&nbsp;the typical required monthly payment range of those who were making payments (<a href="https://www.federalreserve.gov/publications/2020-economic-well-being-of-us-households-in-2019-student-loans-other-education-debt.htm">source</a>)</li>
</ul>




 <h2 id="link-2">Federal Student Loan Statistic</h2>
<h3>By Loan Status (as of March 2020)</h3>
                              
<table class="table">
     <thead>
         <tr>
<td><strong style="color: #fff;">Status</strong></td>
<td><strong># Borrowers</strong></td>
<td><strong>Total Amount</strong></td>
</tr>
</thead>

<tbody>

<tr>
<td>In-School</td>
<td>6.84 million</td>
<td>$134.6 billion</td>
</tr>
<tr>
<td>Grace Period</td>
<td>1.22 million</td>
<td>$23.0 billion</td>
</tr>
<tr>
<td>Repayment</td>
<td>19.48 million</td>
<td>$723.7 billion</td>
</tr>
<tr>
<td>Deferment</td>
<td>3.82 million</td>
<td>$137.3 billion</td>
</tr>
<tr>
<td>Forbearance</td>
<td>3.95 million</td>
<td>$170.9 billion</td>
</tr>
<tr>
<td>Other</td>
<td>0.19 million</td>
<td>$9.9 billion</td>
</tr>
</tbody>
</table>


<h3>By Loan Status (as of March 2020)</h3>
<table class="table">
     <thead>
        <tr>
<td><strong style="color: #fff;">Loan Type</strong></td>
<td><strong># Borrowers</strong></td>
<td><strong>Total Amount</strong></td>
</tr>
   </thead>
<tbody>

<tr>
<td>Stafford Unsubsidized</td>
<td>28.8 million</td>
<td>$528.5 billion</td>
</tr>
<tr>
<td>Stafford Subsidized</td>
<td>29.4 million</td>
<td>$282.9 billion</td>
</tr>
<tr>
<td>Parent PLUS</td>
<td>3.5 million</td>
<td>$99.4 billion</td>
</tr>
<tr>
<td>Grad PLUS</td>
<td>1.4 million</td>
<td>$78.8 billion</td>
</tr>
<tr>
<td>Perkins</td>
<td>1.9 million</td>
<td>$5.6 billion</td>
</tr>
<tr>
<td>Consolidation</td>
<td>11.6 million</td>
<td>$547.7 billion</td>
</tr>
</tbody>
</table>


<h3>By ​Repayment Plan (as of March 2020)</h3>
<table class="table">
<thead>
<tr>
<td><strong style="color: #fff;">Repayment Plan</strong></td>
<td><strong># Borrowers</strong></td>
<td><strong>Total Amount</strong></td>
</tr>    
</thead>
<tbody>

<tr>
<td>Standard</td>
<td>13.60 million</td>
<td>$298.2 billion</td>
</tr>
<tr>
<td>Graduated</td>
<td>3.78 million</td>
<td>$117.7 billion</td>
</tr>
<tr>
<td>Income-Contingent</td>
<td>0.74 million</td>
<td>$35.2 billion</td>
</tr>
<tr>
<td>Income-Based</td>
<td>3.54 million</td>
<td>$196.4 billion</td>
</tr>
<tr>
<td>Pay As You Earn</td>
<td>1.46 million</td>
<td>$107.4 billion</td>
</tr>
<tr>
<td>Revised PAYE</td>
<td>3.10 million</td>
<td>$182.8 billion</td>
</tr>
<tr>
<td>Alternative</td>
<td>1.44 million</td>
<td>$51.2 billion</td>
</tr>
<tr>
<td>Other</td>
<td>N/A</td>
<td>$43.7 billion</td>
</tr>
</tbody>
</table>



<h3>By Debt Size (as of March 2020)</h3>
<table class="table">
<thead>
 <tr>
<td><strong style="color: #fff;">Debt Size</strong></td>
<td><strong># Borrowers</strong></td>
<td><strong>Total Amount</strong></td>
</tr>   
 </thead>   
<tbody>

<tr>
<td>&lt;$5K</td>
<td>7.7 million</td>
<td>$19.6 billion</td>
</tr>
<tr>
<td>$5k &ndash; $10k</td>
<td>7.5 million</td>
<td>$54.3 billion</td>
</tr>
<tr>
<td>$10k &ndash; $20k</td>
<td>9.3 million</td>
<td>$134.3 billion</td>
</tr>
<tr>
<td>$20k &ndash; $40k</td>
<td>9.4 million</td>
<td>$269.1billion</td>
</tr>
<tr>
<td>$40k &ndash; $60k</td>
<td>4.1 million</td>
<td>$203.2 billion</td>
</tr>
<tr>
<td>$60k &ndash; $80k</td>
<td>2.5 million</td>
<td>$175.7 billion</td>
</tr>
<tr>
<td>$80k &ndash; $100k</td>
<td>1.4 million</td>
<td>$121.9 billion</td>
</tr>
<tr>
<td>$100k &ndash; $200k</td>
<td>2.3 million</td>
<td>$310.3 billion</td>
</tr>
<tr>
<td>&gt;$200k</td>
<td>0.9 million</td>
<td>$256.0 billion</td>
</tr>
</tbody>
</table>



<h3>By Borrower Age (as of March 2020)</h3>

<table class="table">
<thead>
<tr>
<td><strong style="color: #fff;">Age</strong></td>
<td><strong># Borrowers</strong></td>
<td><strong>Total Amount</strong></td>
</tr>    
 </thead>   
<tbody>

<tr>
<td>24 or Younger</td>
<td>7.9 million</td>
<td>$120.2 billion</td>
</tr>
<tr>
<td>25 &ndash; 34</td>
<td>14.8 million</td>
<td>$500.1 billion</td>
</tr>
<tr>
<td>35 &ndash; 49</td>
<td>14.1 million</td>
<td>$589.8 billion</td>
</tr>
<tr>
<td>50 &ndash; 61</td>
<td>6.0 million</td>
<td>$252.6 billion</td>
</tr>
<tr>
<td>62+</td>
<td>2.2 million</td>
<td>$81.4 billion</td>
</tr>
</tbody>
</table>



<p>Source:&nbsp;<a href="https://studentaid.gov/data-center/student/portfolio" target="_blank" rel="noreferrer noopener" aria-label=" (opens in a new tab)">Federal Student Aid site</a></p>

<h3>Public Service Loan Forgiveness Statistics (as of September 2018)</h3>
<p>The&nbsp;<a href="https://lendedu.com/blog/public-service-loan-forgiveness/">Public Service Loan Forgiveness program</a>&nbsp;discharges the debt of borrowers who work in the public sector after 120 monthly payments are made.</p>
<p>Here are some statistics about the program:</p>
<ul class="lista">
<li><strong>1,173,420:&nbsp;</strong>Borrowers who requested to certify their loans and employment as eligible for PSLF</li>
<li><strong>890,516:</strong>&nbsp;Borrowers who had their employment and loans certified as eligible for</li>
<li><strong>19,321:&nbsp;</strong>Borrowers who submitted a loan forgiveness application under PSLF</li>
<li><strong>55:&nbsp;</strong>Borrowers who were approved for forgiveness under PSLF</li>
</ul>
<p>Source:&nbsp;<a href="https://www.gao.gov/assets/700/694304.pdf" target="_blank" rel="noreferrer noopener" aria-label=" (opens in a new tab)">United States Government Accountability Office</a></p>


<h2 id="link-3">Private Student Loan Statistics</h2>

<ul class="lista">
<li><strong>$131.81 billion:</strong>&nbsp;the total amount of private student loan debt (<a href="https://cdn2.hubspot.net/hubfs/6171800/assets/downloads/MeasureOne%20Private%20Student%20Loan%20Report%20Q1%202020.pdf" target="_blank" rel="noreferrer noopener">source</a>)</li>
<li><strong>7.87%:</strong>&nbsp;the percentage of total outstanding student debt that is from private student loans (<a href="https://cdn2.hubspot.net/hubfs/6171800/assets/downloads/MeasureOne%20Private%20Student%20Loan%20Report%20Q1%202020.pdf" target="_blank" rel="noreferrer noopener">source</a>)</li>
<li><strong>$11,279:</strong>&nbsp;the average private student loan amount (<a href="https://lendedu.com/blog/state-of-private-student-loans-2020/">source</a>)</li>
<li><strong>8.86%:&nbsp;</strong>the average interest rate on private student loans (<a href="https://lendedu.com/blog/state-of-private-student-loans-report/">source</a>)
<ul>
<li><strong>8.81%:</strong>&nbsp;average variable rate</li>
<li><strong>9.97%:</strong>&nbsp;average fixed rate</li>
</ul>
</li>
<li><strong>21.61%:</strong>&nbsp;the average approval rate for all private student loan applications (<a href="https://lendedu.com/blog/state-of-private-student-loans-2020/">source</a>)
<ul>
<li><strong>36.09%:</strong>&nbsp;average approval rate with a cosigner</li>
<li><strong>8.84%:</strong>&nbsp;average approval rate without a cosigner</li>
</ul>
</li>
<li><strong>748:</strong>&nbsp;the average credit score of approved private student loan applicants (<a href="https://lendedu.com/blog/state-of-private-student-loans-report/">source</a>)</li>
<li><strong>$72,947:&nbsp;</strong>the average income of approved private student loan applicants (<a href="https://lendedu.com/blog/state-of-private-student-loans-report/">source</a>)</li>
<li><strong>2.41%:&nbsp;</strong>percent of private student loans that are 30 to 89 days past due (<a href="https://cdn2.hubspot.net/hubfs/6171800/assets/downloads/MeasureOne%20Private%20Student%20Loan%20Report%20Q1%202020.pdf" target="_blank" rel="noreferrer noopener">source</a>)</li>
<li><strong>1.10%:&nbsp;</strong>percent of private student loans that are 90+ days past due (<a href="https://docs.wixstatic.com/ugd/0aaff0_5a3f9e397ee247e5b33454f5c014c89d.pdf" target="_blank" rel="noreferrer noopener" aria-label=" (opens in a new tab)">source</a>)</li>
<li><strong>Over 100:</strong>&nbsp;the number of private student loan lenders</li>
</ul>






<h2 id="link-4">Graduate Student Loan Statistics</h2>

<ul class="lista">
<li><strong>Average Graduate School Debt:&nbsp;</strong>$57,600* (<a href="https://trends.collegeboard.org/student-aid/figures-tables/cumulative-debt-undergraduate-graduate-studies-time">source</a>)</li>
<li><strong>Average Medical School Debt:&nbsp;</strong>$196,520** (<a href="https://lendedu.com/blog/average-medical-school-debt/">source</a>)</li>
<li><strong>Average Law School Debt:</strong>&nbsp;$48,000 to $340,000** (<a href="https://lendedu.com/blog/average-law-school-debt/">source</a>)</li>
<li><strong>Average Dental School Debt:&nbsp;</strong>$285,184* (<a href="https://lendedu.com/blog/average-dental-school-debt/">source</a>)</li>
<li><strong>Average Vet School Debt:</strong>&nbsp;$143,757* (<a href="https://lendedu.com/blog/average-student-loan-debt-for-veterinarians/">source</a>)</li>
<li><strong>Average Pharmacy School Debt:</strong>&nbsp;$166,528** (<a href="https://lendedu.com/blog/average-pharmacist-student-loan-debt/">source</a>)</li>
</ul>
<p>*Cumulative undergraduate &amp; graduate debt.</p>
<p>**Graduate debt only.</p>









                        </div>

                </div>

                <aside class="col-md-4">

                        <div class="sidebar-content sticky-sidebar">

                                <div class="sticky-side-menu">

                                        <h4>In This Guide</h4>

                                        <ul>

                                                <a href="#link-1"><li>Average Student Loan Debt Statistics</li></a>

                                                <a href="#link-2"><li>Federal Student Loan Statistics</li></a>

                                                <a href="#link-3"><li>Private Student Loan Statistics</li></a>

                                                <a href="#link-4"><li>Graduate Student Loan Statistics</li></a>

                                                

                                        </ul>

                                </div>

                                <div class="special-offer">

                                        <img src="images/offer-1.png" alt="Offer" />

                                        <!-- <h4>Get 10% Off</h4> -->

                                        <a href="student-registration.php"><button type="button" class="btn-apply-inner">Apply Now</button></a>

                                </div>

                        </div>

                </aside>

        </div>

</div>





























<?php include "footer.html" ?>
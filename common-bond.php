<?php include "header.html" ?>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                <p class="my-breadcrumbs">Founded in 2012</p>

                                <h1>Common Bond</h1>

                                <p> Loans are available for undergraduate, graduate, MBA, dental, and medical programs. Loans cover up to 100% of the school’s cost of attendance. </p>

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-12">

                        <div class="inner-main-content-holder">

                                <h2> CommonBond - Founded in 2012 </h2>                                          
                    
 

                                <div class="private-lender-list">

                                        <div class="private-lender-list-name"> </div>

                                        <div class="row private-lender-holder">

                                                <div class="col-md-3 text-center">

                                                        <div class="lender-img-holder">

                                                                <img src="images/commonbond.png" alt="private-lender-img" class="img-responsive" />

                                                        </div>

                                                        <button type="button" class="btn-apply-inner">Visit CommonBond</button>

                                                </div>

                                                <div class="col-md-9">

                                                        <p>CommonBond is one of the leading private lenders that offers private student loans with moderate interest rates and excellent repayment terms. Loans are available for undergraduate, graduate, MBA, dental, and medical programs. Loans cover up to 100% of the school’s cost of attendance. <br>

CommonBond has no application or pre-payment fees and gives repayment terms of 5, 10 or 15 years. The company does a lot of social good. While you are using your loan to finance your education, CommonBond will also support the education for a child in need. The company has funded over $2.5 billion in student loans.

</p>

                                                        <hr>

                                                        <ul class="private-lender-properties">

                                                                <li>Easy-to-fill application process</li>

                                                                <li>Free financial guidance and competitive rates.</li>

                                                                <li>Offer flexible terms and payment options</li>

                                                                <li>Offers financial hardship forbearance</li>

                                                                <li>No application or origination fees</li>

                                                                <li>No prepayment penalties</li>

                                                                <li>Excellent customer service</li>

                                                        </ul>

                                                </div>

                                        </div>

                                </div>
 

                          
       
                                     

                          

                        </div>

                </div>

        </div>

</div>

  <?php include "table-two.php" ?>

<?php include "footer.html" ?>
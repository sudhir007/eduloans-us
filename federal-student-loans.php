<?php include "header.html" ?>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                 <p class="my-breadcrumbs">Student Loans / Federal Student Loans</p>  

                                <h1>Federal Student Loans</h1>

                                <p>Federal student loans are given by the government which help students and parent to borrow money for college directly from the federal government. Federal student loans offer many benefits over any other form of financial assistance to students as it provides the cheapest interest rates and flexible payment schedules. Federal loans have fixed interest rates, so the interest remains the same till you finish paying off the loan, irrespective of how the market rises and falls.</p>

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-8">

                        <div class="inner-main-content-holder">                    


                                
                               <h2 id="link-1">Types of Federal Student Loans</h2>

                               <p>There are four types of federal student loans</p>
                                <h5>Direct Subsidized Loans</h5>
                                <p>Direct Subsidized Loans are for students who prove a financial need as decided by federal regulations. With this type of direct subsidized loan, the school determines the amount based on education-related expenses which a student can borrow. The Government pays the interest on the loan while the student is pursuing studies in school, as well as a 6-month grace period after the student graduates from school.</p>

                                <h5>Direct Unsubsidized Loans</h5>
                                <p>Direct Unsubsidized Loans are available to any student, and there is no need for demonstrating financial need, unlike direct subsidized loans. Even students from wealthier families can borrow direct unsubsidized loans. Unsubsidized loans are low-cost and fixed-rate federal student loans which are available to undergraduate and graduate students. With this type of unsubsidized loan, a student is responsible for paying all the interest on the loan.</p>

                                <h5>Direct PLUS Loans</h5>
                                <p>Direct PLUS Loans are specifically for parents of dependent undergraduate students to supplement their children's aid packages. PLUS loans let parents borrow money to pay for education expenses up to the full cost of attendance. With this type of PLUS loans, it becomes a parent's responsibility to make payments on the loan. If the student decides to make payments on the PLUS loan but fails to pay, then the parents will be held responsible.</p> 

                                <h5>Direct Consolidation Loans</h5>
                                <p>Direct Consolidation Loans  is the type of loan which allows you to combine all your existing federal student loans into a single loan servicer. A Direct Consolidation Loan allows you to consolidate multiple federal education loans into one loan at no cost to you. Through your completion of the free Federal Direct Consolidation Loan Application and Promissory Note, you will confirm the loans that you want to consolidate and agree to repay the new Direct Consolidation Loan. Once the consolidation is complete, you will have a single monthly payment on the new Direct Consolidation Loan instead of multiple monthly payments on the loans you consolidated. </p>



                                <table class="table loan-type-table" id="link-2">

                                        <thead>

                                                <tr>

                                                        <th>Loan Type</th>

                                                        <th>Eligible Student</th>

                                                        <th> URL</th>

                                                </tr>

                                        </thead>

                                        <tbody>

                                                <tr>

                                                        <td>Subsidized Direct Loans</td>
                                                        <td>Undergraduates with financial need</td>
 <td><a href="https://studentaid.gov/understand-aid/types/loans/subsidized-unsubsidized" target="_blank">Learn More</a></td>

                                                </tr>

                                                <tr>

                                                        <td>Unsubsidized Direct Loans</td>
                                                        <td>Undergraduates, graduates, and professional students</td>
 <td><a href="https://studentaid.gov/understand-aid/types/loans/subsidized-unsubsidized" target="_blank">Learn More</a></td>

                                                </tr>

                                                <tr>

                                                        <td>Direct PLUS Loans</td>
                                                        <td>Parents of dependent undergraduate students with no adverse credit history</td>
 <td><a href="https://studentaid.gov/understand-aid/types/loans/plus" target="_blank">Learn More</a></td>

                                                </tr>


                                                       <tr>

                                                        <td>Direct Consolidated loans</td>
                                                        <td>Allows you to consolidate all your loans into a single loan with a single service provider</td>
 <td><a href="https://studentaid.gov/app/launchConsolidation.action" target="_blank">Learn More</a></td>

                                                </tr>

                                        </tbody>

                                </table>

                        </div>

                </div>

                <aside class="col-md-4">

                        <div class="sidebar-content sticky-sidebar">

                                <div class="sticky-side-menu">

                                        <h4>In This Guide</h4>

                                        <ul>

                                                <a href="javascript:void(0);"><li>Federal Student Loans</li></a>

                                                <a href="private-student-loans.php"><li>Private Student Loans</li></a>

                                                <a href="private-student-loan-refinancing.php"><li>Private Student Loan Refinancing</li></a>

                                                <a href="javascript:void(0);"><li>Deciding How Much to Borrow</li></a>

                                                <a href="javascript:void(0);"><li>How to Get Federal Student Loans</li></a>

                                        </ul>

                                </div>

                                <div class="special-offer">

                                        <img src="images/offer-1.png" alt="Offer" />

                                        <!-- <h4>Get 10% Off</h4> -->

                                        <a href="student-registration.php"><button type="button" class="btn-apply-inner">Apply Now</button></a>

                                </div>

                        </div>

                </aside>

        </div>

</div>





























<?php include "footer.html" ?>
<?php include "header.html" ?>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                <p class="my-breadcrumbs">Partner with us / Sallie Mae</p>

                                <h1>Sallie Mae</h1>

                                <p>Sallie Mae is a consumer bank that helps families to borrow private education loans

                                        to fulfil the dream of higher studies into reality. Set up in 1972, it is the

                                        largest provider of private education loans in the United States.<br>

                                        Sallie Mae is the biggest student lender in the market today that offers

                                        undergraduate and graduate student loans with fixed and variable interest rates.

                                        It has been helping students and their families find the money they need to make

                                        the dream of a quality education a reality and manages more than 25 million

                                        borrowers across the U.S.</p>

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-12">

                        <div class="inner-main-content-holder">

                                <h2>Why choose Sallie Mae for student loans </h2>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> 100% coverage for all school-certified expenses like tuition, fees, books, housing, meals, travel, and even a laptop.</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No origination fees</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No penalty for early repayment</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Offers competitive interest rates</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Cost-saving features</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Provides multiple repayment options on

                                        loans for students</div>

                        </div>

                </div>

        </div>

</div>

<div class="inner-middle-bg">

        <h4>Sallie Mae helps students get to their dream school</h4>

        <button type="button" class="btn-apply-inner">Apply Now</button>

</div>

<?php include "table-two.php" ?>

<?php include "footer.html" ?>
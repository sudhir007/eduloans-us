<?php include "header.html" ?>
<div class="container">
        <div class="row">
                <div class="col-md-12">
                        <div class="page-intro">
                                <p class="my-breadcrumbs">FAQ</p>
                                <h1>Frequently Asked Questions</h1>
                                <p>How can we help you?</p>
                        </div>
                </div>
        </div>
        <div class="panel-group faq-page" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What is the difference between  Private student loans & federal student loans ?
                                </a>
                                </div>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        <p>Federal student loans are government loans  provided directly by the U.S. Department of Education. They do not bother about your credit history and hence are more need based. The loans issued in a single year have the same interest rate. The federal loans have fixed rate of Interest Vs Private loans who have Varied Rate of Interest  Once taken out, rates on federal student loans are fixed for life.</p>
                                        <p>Federal loans are through an application submitted to FAFSA where as private loans can be provided by multiple lenders. At the Undergraduate level federal loans tend to be very cost effective. At the Graduate level they vary as per the financial constraints of the federal government. You can qualify for federal student loans by submitting a Free Application for Federal Student Aid (the “FAFSA”).</p>
                                        <p>Private loans have Fixed or Variable Interest rate, Varied tenure and flexible mode of repayment however they can never be forgiven or deferment of loan repayment option taken like Federal student loans.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                                <div class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        How much money can I borrow with a private student loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                        <p>A private student loan looks at the student profile and the credit history of the Co-signor. Student loans are futuristic loans where in the earnings potential post the program is the key matrix of ascertaining the loans. With a private student loan the student is eligible to borrow 100% of your college expenses including Tuition Fees, Living Expenses, Books etc. The Federal student loans amount usually gets deducted in the eligibility calculation of the student. The eligibility further varies by lender and, as is the case with federal student loans, can include annual or cumulative borrowing limits. Other private lender criteria that can affect how much you can borrow include your credit history, the credit quality of your co-signer, your school’s cost of attendance, the degree you’re earning and your corresponding expected income with that degree.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                                <div class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        How to choose the best private Student Loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                        <p>We need to look at picking the most suited student  loan to your needs. There are multiple parameters you should look at when finalizing your loan</p>
                                        <ul type="disc">
                                                <br>
                                                <li><b>Loan Types -</b>  Private loans are available for all types of courses and all levels of courses. We have loans for Medical School, Medical Residency, Dental School, Dental Residency, Health professional graduate, Law school, Bar Study and Career training. We have loans for K12, Undergraduate, Graduate loans. The individual lenders determine the eligibility of the loan based on the expected earning after the program. The interest rate would vary with the course selected and private loan providers have there own analysis about the University and program. </li>
                                                <li><b>Rate Types -</b>  There are 2 types of Interest rates - Fixed interest rate and Variable interest rate.Fixed interest rate is when the loan is approved and there is a fixed interest rate for the entire tenure of the loan. The Variable interest rate is dependent on LIBOR and fluctuates generally monthly, quarterly or yearly. The variable interest also dynamically varies with your credit score which would vary year on year. In all probability with improving disposable income as you progress in your career many student lenders have the concept of refinancing the loan at lower rates compared to your original loan offer provided by the student loan lender.  In most cases in 2019 the Fixed rate offered was  generally higher than variable rate offered for the same profile. </li>
                                                <li><b>Loan Term -</b>  Students have the flexibility of having loan term as per their suitability.  Generally the tenure is 5 years, 10 years , 15 years to a maximum of 20 years. Generally increase in tenure of the loan increases the interest rate for the loan. We need to be very careful in choosing the tenure where in we look to repay the interest rate as soon as possible without the probability of default. We need to ascertain our own financial requirements and then choose the best possible tenure. In some lenders like Discover they offer like a blanket tenure of 20 years where you have the flexibility of prepayment. In all lenders you have the option of prepayment without any additional cost. </li>
                                                <li><b>Loan Amount -</b>  Loan amount varies with different lenders as per their product paper. In most cases a minimum of USD 1000 upto the total requirement for your studies or maximum of USD 350,000.In case the  increase in loan amount leads to reduction of drawing power for the Co-signer, which leads to an increase in interest rate.  MOre leveraged the co-signer more is the risk associated with the loan and hence the higher the interest rate.  as the eligibility of the co-signer and repayment potential is tested. Its best advised only to sanction the amount required and not sanction the entire amount and avail of the loan. </li>
                                                <li><b>Application & Origination Fees -</b>  Generally for all  private student loans their is no Application or origination fees. </li>
                                                <li><b>Discounts -</b>  Many private student lenders provide discount in the form of Auto pay where in they provide between 0.25% to 0.50% rebate incase the student sets up an autopay from the account. This actually ensures timely payment and is very important to maintain a healthy credit rating. There are other discounts like loyalty discount where in if you do other services with the bank they provide a further reduction of interest rate by 0.25% to 0.50%. The other kind of discounts are 1% Cash Back offer after one starts successful working, Refer a friend where in you can get USD 200 - 500 cash back from the lenders,etc.</li>
                                                <li><b>Repayment OPtions -</b>  Private student loans generally have four types of payment options. In-school interest only option signifies that the student  is repaying only the interest drawn on the student loan. The same is generally on the amount drawn and not fixed for the entire course period. Iin-school fixed payment is a fixed nominal payment requested at the time the student is in school . The above may vary from USD 25 to as much as USD 1000 per month. The third type is  in-school deferment option where in the student can choose to start paying after he has passed out of the college. They also have generally a moratorium period for 6 Months to 1 year. This is very flexible for the student and he is not required to pay anything during study. In this case as the interest is compounded the principle outstanding increases significantly and the student ends up paying a higher equated monthly installment. The last option is generally immediate repayment where in the student starts paying both the interest and principal as an Equated monthly payment. </li>
                                                <li><b>Deferment or forbearance hardship options -</b> For federal loans you can look at both Deferment which can last for maximum upto 36 MOnths and Forbearance for 12 months. Private student loans are not obligated to either of them but some providers do provide an option in case the candidate has expired, moved to active military service,etc.</li>
                                                <li><b>Cosigner release -</b>  Cosigner release is a feature provided by private student loan players where in after certain months once the student has started earning the Co-signer can be removed from the loan structure. This is usually possible after certain designated months of undelayed and full payment of Equated monthly installment. In most cases the student earning at that time is taken into consideration and the Cosigner is released. This is very good as then the cosigner who generally is family or a close friend can utilise his credit profile for his personal use. </li>
                                                
                                        </ul>
                                        
                                        <p>With so many variables at play Picking the right and the best education loans can make a big difference when time comes to repay your debt from school or college. If you are unsure of what type of education loan to choose, a good rule of thumt is to select one that offers a low-interest rate, flexible repayment options and borrower protections. </p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What are the different types of Education Loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                        <p>Education loans are meant to provide monetary assistance to students to meet the expenses associated with their studies. There are a variety of education loans, but they are broken down into two basic types- federal student loans and private student loans.</p>

                                        <p>The key difference between federal and private student loans is that federal student loans are provided by the Government, while private student loans are provided by a private-sector lender such as a bank, credit unions and other financial institutions. Both federal and private student loans offer very different benefits, interest rates, and repayment options.</p>

                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        Steps for education loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                        <p>Education loans have made the dream of pursuing academic courses easier without burdening parent or borrowers with liabilities. The education loan process may differ from bank to bank, but there are some fundamental steps. To avail a loan, the applicant needs to fill out a loan application form, choose loan offer, provide validated supporting documents and wait for loan approval. Once approved, a bank or lender will disburse the loan. Today, one can apply for an education loan online.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        Which are the major players involved with Refinancing?
                                </a>
                                </div>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">
                                        <p>Refinancing is when you replace an existing loan with a new loan. The same is done either to consolidate your loan to more payable EMI by adjusting the Duration or to reduce the interest rate. Refinancing an existing student loan can seriously reduce your monthly payments and even the total cost of your loan. As you compare refinancing your student loans from multiple lenders, you could consider these lenders- Earnest, College Ave, Citizens Bank for competitive rates and additional benefits. You can go with one of these or find another lender to get the best deal that fits your needs.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        Steps in Refinance your loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                        <p>If you are struggling with student loan debt or wish to make it more manageable, refinancing your student loans can help your financial situation. You will need to follow these four steps for refinancing.</p>
                                        <ul>
                                                <li>Gather information on the loans to refinance</li>
                                                <li>Apply for student loan refinancing</li>
                                                <li>Read promissory note carefully and sign your application</li>
                                                <li>Wait for your application to be evaluated by the lender</li>
                                                <li>Once you’re approved, your new lender will pay off old loans and you’ve a new refinanced student loan at a lower interest rate</li>
                                        </ul>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        Why should you refinance your loan? 
                                </a>
                                </div>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                <div class="panel-body">
                                        <p>When you refinance you loan, you find a lender who pays off your existing loans with a new one at a lower interest rate. Refinancing your loan will save you money on interest costs as it cut the amount of interest you pay over time.</p>
                                        <div>Comparison for refinance</div>
                                        <div class="table-responsive">
                                                <table class="table loan-type-table">
                                                        <thead>
                                                                <tr>
                                                                        <th>Lender</th>
                                                                        <th>Best For</th>
                                                                        <th>Fixed APR</th>
                                                                        <th>Variable APR</th>
                                                                        <th>Know More</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <tr>
                                                                        <td>Sallie Mae</td>
                                                                        <td>Part-time students & those who want repayment flexibility</td>
                                                                        <td>4.74% –11.35%</td>
                                                                        <td>2.87% – 10.33%</td>
                                                                        <td><a href="#">Check Rate</a></td>
                                                                </tr>
                                                                <tr>
                                                                        <td>Ascent</td>
                                                                        <td>Independent students; faster payoff</td>
                                                                        <td>4.02% –12.93%</td>
                                                                        <td>3.31% – 12.62%</td>
                                                                        <td><a href="#">Check Rate</a></td>
                                                                </tr>
                                                                <tr>
                                                                        <td>Earnest</td>
                                                                        <td>International student loans with co-signer</td>
                                                                        <td>3.45% – 6.99%</td>
                                                                        <td>1.99%– 6.89%</td>
                                                                        <td><a href="#">Check Rate</a></td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What are private students’ loans?
                                </a>
                                </div>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                <div class="panel-body">
                                        <p>A private student loan is a credit-based education loan for college offered by private lending institutions, such as banks and credit unions or other lenders rather than the federal government. The loan can cover the gap between financial aid received and the full cost of attendance. The loan is based on the college and credit history of the co-signer. The loan is not available to everyone and carries a high interest rate.  The loan is the best option for students and parents who still can’t cover the cost of College through personal funds or federal loan.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTen">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What is the difference between Private Student Loans & Federal Student Loans ?
                                </a>
                                </div>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                <div class="panel-body">
                                        <p>Federal student loans are government loans provided directly by the U.S. Department of Education. They do not bother about your credit history and hence are more need-based. The loans issued in a single year have the same interest rate. The federal loans have a fixed rate of Interest Vs Private loans which have Varied Rate of Interest Once taken out, rates on federal student loans are fixed for life.<br>
Federal loans are through an application submitted to FAFSA, whereas private loans can be provided by multiple lenders. At the Undergraduate level, federal loans tend to be very cost-effective. At the Graduate level, they vary as per the financial constraints of the federal government. You can qualify for federal student loans by submitting a Free Application for Federal Student Aid (the “FAFSA”).<br>
Private loans have Fixed or Variable Interest rate, Varied tenure and flexible mode of repayment; however, they can never be forgiven or deferment of loan repayment option taken like Federal student loans.
</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEleven">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        Who is a Co signor? What is a Cosigner Vs Non-Cosigner Loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                <div class="panel-body">
                                        <p>A cosigner is a person who applies for a loan with you. A cosigner is considered as a co-borrower and is equally responsible for paying back the loan if you do not make payments or stop making payments for any reason. Having a cosigner gives your lender additional assurance that the loan will be paid.<br>
A non-cosigner loan is an education loan available for students without a cosigner. All federal student loans that are awarded on the basis of financial need do not require the borrower to have a cosigner. 
</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwelve">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What is the Average Earning for a Cosigner?
                                </a>
                                </div>
                        </div>
                        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                                <div class="panel-body">
                                        <p>The Average eligibility of a co-signor is determined by the concept of FOIR. FOIR stands for fixed income obligation ratio wherein we look at the free income available to the family after deduction of Taxes, Expenses and other loan repayment expenses. Most banks look around 60 -80% FOIR. IN the education loan on most occasions the future earning potential of the student is taken into consideration. Hence the concept of average earning becomes dependent on various factors.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThirteen">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="true" aria-controls="collapseThirteen">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What is the average credit rating for a Cosigner?
                                </a>
                                </div>
                        </div>
                        <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                <div class="panel-body">
                                        <p>The credit Score FICO ranges from a low of 300 to a high of 850 – a perfect credit score which is achieved by only 1% of consumers. The national average is around 690 and a good credit score is above 720. An excellent credit score is 800.  The Co-signor generally needs to have a credit score of is 720 or higher.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFourteen">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="true" aria-controls="collapseFourteen">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What is difference between Fixed and Variable Interest rates?
                                </a>
                                </div>
                        </div>
                        <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
                                <div class="panel-body">
                                        <p>As the name implies, a fixed-rate loan has a static interest rate for the whole loan term. Your repayments stay the same every month in a fixed interest rate, which provides more certainty because you know how much you will pay every month. In variable rate loan, the interest rate changes over time. The variable rate is dependent on LIBOR and the Fed rate. The LIBOR is always taken as the base rate which has no risk and all percentage above the LIBOR is your risk premium. Generally, the risk premium remains the same over the tenure of the loan however the LIBOR keeps changing every day. Generally, in most lenders the rate changes every 3 months. It may go up and down during your loan term.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFifteen">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="true" aria-controls="collapseFifteen">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What are the key points to consider while selecting student loan providers?
                                </a>
                                </div>
                        </div>
                        <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
                                <div class="panel-body">
                                        <p>There are now plenty of student loan lenders on the market to choose from, but all lenders are not created equal. Taking a student loan is a big decision, so choosing the lender that is right for you becomes crucial.<br><br>While selecting for a lender consider these factors:</p>
                                        <ul>
                                                <li><strong>Check for low-interest rates:</strong> Rates affect how quickly your balance grows, and how much you will be paying overall.</li>
                                                <li><strong>Look for flexible or generous repayment terms:</strong> It helps you pay your monthly loan payments with ease. Also, look for any application or origination fees.</li>
                                                <li><strong>Check for lender experience and reputation:</strong> Find out how long has the lender been in business and check out customer reviews on lenders and verify their Better Business Bureau rating.</li>
                                                <li><strong>Look for quality of customer service:</strong>  Find a lender that provides the service on time and the personal assistance to get answers to your questions.</li>
                                        </ul>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSixteen">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="true" aria-controls="collapseSixteen">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What are the types of Scholarships available to study?
                                </a>
                                </div>
                        </div>
                        <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
                                <div class="panel-body">
                                        <p>Government Scholarships, Merit-based scholarships, Need-based Scholarships, Country of origin specific, Minority scholarships, Subject-based scholarships, Program-specific awards are types of scholarships meant for studying.</p>
                                </div>
                        </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeventeen">
                                <div class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen" aria-expanded="true" aria-controls="collapseSeventeen">
                                        <i class="more-less fas fa-chevron-down"></i>
                                        What if you default the loan?
                                </a>
                                </div>
                        </div>
                        <div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
                                <div class="panel-body">
                                        <p>Defaulting on a loan happens when repayments are not made on time or for a certain period of time. Just missing a handful of payments can result in the account being placed in default status. Defaulting will severely reduce your credit score, impact your ability to receive future credit, bring financial penalties, litigation and lead to the seizure of personal property.</p>
                                </div>
                        </div>
                </div>
        </div><!-- panel-group -->
                
                
</div><!-- container -->
  <?php include "table-one.php" ?>
<?php include "footer.html" ?>
                
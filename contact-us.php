<?php include "header.html" ?>
<main id="main">
	<!-- contact block -->
	<section class="contact-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-intro">
						<p class="my-breadcrumbs">Contact Us</p>
						<h1>Contact Details</h1>
						<p>Welcome to our Website. We are glad to have you around.</p>
					</div>
				</div>
			</div>
			<div class="row my-marginer-2">
					<h2 class="text-center my-section-header">Our Offices</h2>
					<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="widget widget_text" style="margin-bottom: 0px;">
									<h3>Corporate Office <span>(USA)</span></h3>
									<address>
											<div>16192, Coastal Highway, Lewes, Delaware</div>
											<div><strong>Email:</strong>banit.s@eduloans.org</div>
											<div><strong>Tel:</strong> +1 3473288189</div>
									</address>
							</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
							
							<div class="widget widget_text" style="margin-bottom: 0px;">
									<h3>Head &amp; Regional Office (Indian Subcontinent)</h3>
									<address>
											<div>Office no 32/33, 2nd Floor, Dheeraj Heritage, S.V. Road, Santacruze (W), Mumbai 400050</div>
											<div><strong>Tel:</strong>+91 9004082734 / +91 9819919363</div>
									</address>
							</div>
					</div>
			</div>
			<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
							
							<div class="widget widget_text" style="margin-bottom: 0px;">
									<h3>Regional Office (South East Asia)</h3>
									<address>
											<div>1, Coleman Street, The Adelphi, #05 -06 A, Singapore - 179803</div>
											<div><strong>Tel:</strong>+65 97650979</div>
									</address>
							</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
							
							<div class="widget widget_text" style="margin-bottom: 0px;">
									<h3>Regional Office MENA &amp; Africa</h3>
									<address>
											<div>817, Emirates Islamic Building RAK, UAE , PO Box: 37655</div>
											<div><strong>Tel:</strong>+9715 08585454</div>
									</address>
							</div>
					</div>
			</div>
			<div class="row my-marginer-2">
				<!-- <h2 class="text-center my-section-header">Our Offices</h2> -->
				<!-- <div class="col-xs-12 col-sm-12 col-md-12">
					<div class="widget widget_text" style="margin-bottom: 0px;">
						<h3>Corporate Office <span>(USA)</span></h3>
						<div class="row">
							<div class="col-md-4">
								<div class="my-padding-top"><i class="fa fa-map-pin my-icon-style" aria-hidden="true"></i></div>
								<div class="my-padding-top"><strong>16192, Coastal Highway, Lewes, Delaware</strong></div>
							</div>
							<div class="col-md-4">
								<div class="my-padding-top"><i class="fa fa-envelope my-icon-style" aria-hidden="true"></i></div>
								<div class="my-padding-top"><strong>banit.s@eduloans.org</strong></div>
							</div>
							<div class="col-md-4">
								<div class="my-padding-top"><i class="fa fa-phone my-icon-style fa-flip-horizontal" aria-hidden="true"></i></div>
								<div class="my-padding-top"><strong>+1 3473288189</strong></div>
							</div>
						</div>
					</div>
					
				</div> -->
				
				<!-- <hr class="sep-or element-block" data-text="or"> -->
				<!-- contact form -->
				<div class="col-xs-12 col-sm-12 col-md-12">
					<form action="#" class="contact-form my-marginer-2">
						
						<h2 class="text-center my-section-header">Drop Us a Message</h2>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control element-block" placeholder="First Name">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control element-block" placeholder="Last Name">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<input type="email" class="form-control element-block" placeholder="Email">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<input type="tel" class="form-control element-block" placeholder="Phone">
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<textarea class="form-control element-block" placeholder="Message"></textarea>
								</div>
							</div>
						</div>
						<div class="text-center">
							<button type="submit" class="btn-submit-contact">Send Message</button>
						</div>
						
					</form>
				</div>
				
			</div>
			<!-- mapHolder -->
		<!-- 	<div class="mapHolder">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7540.829892473072!2d72.8369!3d19.089444!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x29ca30c80c126627!2sEduloans%20-%20Empowering%20Dreams!5e0!3m2!1sen!2sus!4v1574941603643!5m2!1sen!2sus" style="border:0" allowfullscreen="" width="100%" height="400" frameborder="0"></iframe>
			</div> -->
			<!-- btn aside block -->
			<aside class="btn-aside-block container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col">
						<h3>Have Any Questions?</h3>
						<p>Visit our support page to get your questions answered</p>
					</div>
					<div class="col-xs-12 col-sm-4 text-right col">
						<a href="#"><button class="btn-submit-contact">Get Support</button></a>
					</div>
				</div>
			</aside>
		</section>
	</main>
	<?php include "footer.html" ?>
<?php include "header.html" ?>

<div class="container">
        <div class="row">
                <div class="col-md-12">
                        <div class="blog-page-intro blog-img-1">
                        </div>
                </div>
        </div>

        <div class="row blog-content">
                <h1 class="ext-center">Why Women Carry More Student Debt than Men ? </h1>

                <div class="col-md-8">

 <div class="inner-main-content-holder">
 


 
 <p><strong>Citizens Bank</strong></p>
 <img src="images/citizen.jpg">
<p>Citizens Bank offers fixed- and variable-rate private student loans for undergraduate and graduate degrees as well as parent loans. Borrowers can get approved for multiple years of student loans.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: U</strong>ndergrad, grad, parent, refinance for students, refinance for parents</li>
<li><strong>Rate types: </strong>Fixed and variable</li>
<li><strong>Loan terms: </strong>Five to 15 years</li>
<li><strong>Loan amounts: </strong>$1,000 to $350,000</li>
<li><strong>Application or origination fees: </strong>Save with no application, origination, or disbursement fees.</li>
<li><strong>Discounts:</strong> Loyalty, autopay</li>
<li><strong>Repayment options:</strong> Make full or interest-only payments while in school or wait until after graduation</li>
<li><strong>Deferment or forbearance hardship options:</strong> Economic hardship, military service, post active duty, enrolled or returning to school</li>
<li><strong>Co-signer release: </strong>Can apply for a co-signer release after 36 consecutive on-time principal and interest payments</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p>Best Lender for Exclusively Offering Student Loans</p>
 <img src="images/college.jpg">
<p> <strong>College Ave</strong></p>
<p>College Ave Student Loans offers student loans to borrowers in all 50 states. Undergraduate, graduate and parent loans are available. The lender specializes in simple applications with an instant decision.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: </strong>Undergraduate, Graduate, Parent Loans, Refinancing, Parent Refinancing, MBA, Law, Dental, Medical and International</li>
<li><strong>Loan terms: </strong>5 to 15 years in school for undergrad/grad/parent, 5 to 20 years medical, dental and law schools</li>
<li><strong>Loan amounts: </strong>$1,000 to up to 100% of the student&rsquo;s school-certified cost of attendance.</li>
<li><strong>Application or origination fees: </strong>If a monthly payment is not made within 15 days of the due date, you will be assessed a late charge equal to 5% of the unpaid amount of the monthly payment or $25, whichever is less.</li>
<li><strong>Discounts:</strong> 0.25% auto-pay interest rate reduction with valid bank account designated for required monthly payments</li>
<li><strong>Deferment or forbearance hardship options:</strong> Yes - case by case</li>
<li><strong>Co-signer release: </strong>Yes, if borrower has made 24 consecutive payments on time, with no periods of forbearance or deferment.</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p><a href="https://www.collegeavestudentloans.com/lp/undergrad-student-loans/?utm_campaign=affiliate%20ongoing&amp;utm_source=redventures&amp;utm_medium=aggregator&amp;utm_content=usnews&amp;brand=college%20ave&amp;product=inschool&amp;program=undergrad&amp;p_aff=redventures&amp;goal=acquisition&amp;agg_ref_id=PLACEHOLDER"> Learn More</a></p>
<p>Best Lender for No Application, Origination Or Late Fees</p>
<p> <strong>Discover</strong></p>
<img src="images/discover.jpg">
<p>Discover offers personal loans for debt consolidation, home improvement and major purchases. Loan terms from three to seven years are available.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: </strong>Undergraduate, Graduate, MBA, Law, Dental, Medical, International, International students require a cosigner who is a US citizen or permanent resident. Health Professions Loans, Residency Loans, Bar Exam Loans and Consolidation Loans, refinance</li>
<li><strong>Rate types: </strong>Fixed and variable</li>
<li><strong>Loan terms: </strong>Up to 15 years for undergraduates; 20 years for graduates; 10 or 20 years for consolidation</li>
<li><strong>Loan amounts: </strong>$1,000 to total cost of attendance minus other financial aid</li>
<li><strong>Application or origination fees: </strong>No application, origination or late fees</li>
<li><strong>Discounts:</strong> Customers can lower their student loan interest rate by 0.25% while enrolled in automatic payments. Students can also receive an interest rate discount of 0.35% if they select the interest-only repayment option and make interest-only payments during the in-school and grace periods.</li>
<li><strong>Deferment or forbearance hardship options:</strong> Deferment or forbearance available</li>
<li><strong>Co-signer release: </strong>N/A</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p>Best Lender for Borrowers With a FICO Credit Score As Low As 650</p>
<p> <strong>Earnest</strong></p>
<img src="images/earnest.jpg">
<p>Earnest was founded in 2013 and has funded more than $4.5 billion in student loan refinancing to about 50,000 borrowers. You may be approved for a loan with a debt-to-income ratio of up to 65%.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: </strong>Undergraduate, Graduate, Cosigned Private Loans, Business School Loans, Medical School Loans, Law School Loans, Refinance Student Loans</li>
<li><strong>Rate types: </strong>Fixed and variable</li>
<li><strong>Loan terms: </strong>N/A</li>
<li><strong>Loan amounts: </strong>$1,000 to your total cost of attendance, minus other financial aid. Aggregate loan limits apply.</li>
<li><strong>Application or origination fees: </strong>No application fee, origination fee not disclosed</li>
<li><strong>Discounts:</strong> 0.25% Autopay</li>
<li><strong>Deferment or forbearance hardship options:</strong> Yes. Earnest has multiple options for students having difficulties making payments, including Skip-a-payment, Forbearance, Deferment, Rate Reduction Program, and Term &amp; Rate Modification Program.</li>
<li><strong>Co-signer release: </strong>Yes, if refinancing through Earnest, you can release the cosigner of previous loan if applying as an individual</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p>Best Lender With a Referral Bonus Available</p>
<p> <strong>Education Loan Finance</strong> </p>
<img src="images/edu-loan.jpg">
<p><a href="https://loans.usnews.com/reviews/educationloanfinance-student">Education Loan Finance</a> has offered student loan refinancing since 2015. A division of SouthEast Bank, the management team has more than 30 years of experience in student loans. The lender offers loan terms from five to 20 years.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: </strong>Undergraduate, Graduate, Parental, Refinance</li>
<li><strong>Rate types: </strong>Fixed and variable</li>
<li><strong>Loan terms:</strong> 5 to 15 years</li>
<li><strong>Loan amounts: </strong>$10,000 or higher</li>
<li><strong>Application or origination fees: </strong>No application or origination fees</li>
<li><strong>Discounts:</strong> Auto-pay discount is reflected in approved interest rate, as all borrowers are required to make electronic or digital transfer payment.</li>
<li><strong>Deferment or forbearance hardship options:</strong> N/A</li>
<li><strong>Co-signer release: </strong>N/A</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p>Best Lender With No Credit History Required</p>
<p> <strong>MPower Financing</strong></p>
<img src="images/mpower.png">

<p>MPower offers student loans in the U.S. and Canada to international students without co-signers, collateral or credit history. The lender considers academic success and career path when making credit decisions.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: </strong>Undergrad, graduate, international refinancing</li>
<li><strong>Rate types: </strong>Fixed</li>
<li><strong>Loan terms: </strong>10 years</li>
<li><strong>Loan amounts: </strong>$2,000 to $50,000</li>
<li><strong>Application or origination fees: </strong>There is no fee to apply for a loan. If you are approved and choose to proceed with the loan, a 5.0% origination fee will be added to your loan balance and must be repaid over the term of your loan. You are not required to pay this fee upfront; instead, you will pay it back with your monthly loan payments. If you decide to repay part or all of your loan ahead of schedule, there is no penalty.</li>
<li><strong>Discounts:</strong> Autopay, on-time payment, reporting proof of graduation and employment</li>
<li><strong>Deferment or forbearance hardship options:</strong> Not disclosed</li>
<li><strong>Co-signer release: </strong>Our loan process was created so that students wouldn&rsquo;t have to worry about having a co-signer or credit score.</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p>Online Student Loans</p>
<p><a href="https://loans.usnews.com/reviews/sofi-student"><strong>SoFi</strong></a></p>
<img src="images/sofi.jpg">
<p>SoFi has offered student loans since 2011. The lender has served more than 250,000 borrowers with more than $18 billion in student loan refinancing. Borrowers with a credit score as low as 650 may be approved. You can apply for student loan refinancing online with SoFi and get an instant decision.<br /> <br /> <strong>Highlights</strong></p>
<ul>
<li><strong>Loan types: </strong>Undergraduate, graduate, MBA, law, dental, medical, parent, refinancing, parent refinancing</li>
<li><strong>Rate types: </strong>Fixed and variable</li>
<li><strong>Loan terms: </strong>5 to 15 years for private student loans, 5 to 20 years for student loan refinance</li>
<li><strong>Loan amounts: </strong>$5,000 to not disclosed</li>
<li><strong>Application or origination fees: </strong>None</li>
<li><strong>Discounts:</strong> 0.25% AutoPay discount is available. If you take out a SoFi loan, you could be eligible for a 0.125% discount on another SoFi loan product.</li>
<li><strong>Deferment or forbearance hardship options:</strong> Yes, options available</li>
<li><strong>Co-signer release: </strong>Yes; borrowers can release cosigners with at least 24 months of full principal and interest payments. Terms and conditions apply.</li>
<li><strong>BBB rating:</strong> A+</li>
</ul>
<p><a href="https://www.sofi.com/refer/411/113463?subid=a9881577ae11b2e0015f"> Learn More</a></p>
<p><strong>How Can Students Maximize Federal and Free Financial Aid?</strong></p>
<p>Before you consider private student loans, you should make the most of federal aid and free financial aid, including private scholarships.</p>
<p>&ldquo;Your first step in financing your education is to submit a Free Application for Federal Student Aid, commonly called a FAFSA,&rdquo; says Jay S. Fleischman, a lawyer who advises student loan borrowers on effective repayment strategies. Even if you don&rsquo;t think you&rsquo;ll need financial assistance, or think you won&rsquo;t qualify, it&rsquo;s worth filling out and submitting a FAFSA.</p>
<p>The <a href="https://studentaid.ed.gov/sa/fafsa">FAFSA</a> is the key to most financial aid. It&rsquo;s a requirement for the student financial assistance programs authorized under Title IV of the Higher Education Act. These include federal loans, grants and work-study programs. There&rsquo;s no income cutoff or grade requirement for some types of federal aid, which are <a href="https://studentaid.ed.gov/sa/sites/default/files/financial-aid-myths.pdf">common myths</a>.</p>
<p>Though it&rsquo;s primarily used for federal aid, the FAFSA is often necessary for other forms of financial aid.</p>
<p>&ldquo;Many states and colleges use your FAFSA information to determine your eligibility for state and school aid,&rdquo; Fleischman points out. There are private scholarships and grants that require applicants to complete the FAFSA.</p>
<p><strong>How Do Private Student Loans Work?</strong></p>
<p><br /> Federal students loans offer standardized loan types, interest rates and terms to most borrowers. With private student loans, your options and interest rate will vary, though there are some laws that affect all private student loans. Your credit, and that of a co-signer if you have one, will also impact what types of loans you qualify for and the interest rate you&rsquo;ll receive.</p>
<p><strong>Loan Types</strong></p>
<p>Lenders may offer different types of loans depending on the degree you&rsquo;re pursuing. The loan type can affect the total possible loan amount, interest rate and repayment terms.</p>
<ul>
<li><strong>Community college or technical training. </strong>Some lenders offer loans to students who are pursuing a two-year degree, attending a nontraditional school or are going to a career-training program.</li>
<li><strong>Undergraduate school loans. </strong>You can take out undergraduate school loans to pay for expenses while pursuing a bachelor&rsquo;s degree. Undergraduate loans may have lower interest rates and higher loan limits than community college loans.</li>
<li><strong>Graduate or professional school loans. </strong>Graduate school loans tend to have higher maximum loan amounts than undergraduate loans, reflecting the higher cost of attending school for a master&rsquo;s degree or doctorate. Some lenders have special loan programs for business, law or medical school.</li>
<li><strong>Parent loans. </strong>Parent loans are offered to parents of students. Some families have an informal agreement that the child will make loan payments after graduating, but with a parent loan, the legal responsibility to repay the loan falls on the parent.</li>
</ul>
<p><strong>Loan Terms</strong></p>
<p>The loan term is the length of the loan&rsquo;s repayment period, which could range from five to 20 years for private student loans. Typically, shorter loans have higher monthly payments, lower interest rates and lower total costs. Longer loans have lower monthly payments, but higher interest rates and higher total costs.</p>
<p><strong>Loan Limits</strong></p>
<p><strong>Loan minimums:</strong> Most lenders have a minimum amount you can borrow. Although it could be as low as $1,000, a private student loan may not be the best option if you only need a few hundred dollars for books. Loan minimums may vary depending on your state of permanent residence.</p>
<p><strong>Loan maximums:</strong> Lenders can have several limits that impact how much you can borrow. There could be a maximum annual or total amount you can borrow from that lender. Or, there could be a maximum aggregate private and student loan amount you must be under to qualify for a loan. The maximum loan limits may be higher if you&rsquo;re going to graduate, professional or medical school, reflecting these programs&rsquo; potentially higher cost.</p>
<p>Regardless of the other limits, you also may be limited to borrowing up to your school&rsquo;s certified cost of attendance minus the other financial aid you received.</p>
<p><strong>Interest Rate Types</strong></p>
<p>Lenders offer student loans with either a fixed or adjustable interest rate. You may not be able to switch your interest rate type after taking out a loan, so carefully consider your options before deciding.</p>
<p>When you&rsquo;re comparing student loans from different lenders, you should look at the annual percentage rate rather than the interest rate. The APR is your total cost of borrowing each year, which takes into account the loan&rsquo;s interest rate, how often your interest compounds, and other fees or discounts.</p>
<p><strong>Fixed-rate loans. </strong>With a fixed-rate private student loan, your interest rate is set when you take out the loan and it won&rsquo;t change over the life of the loan. The rate you lock in can depend on market rates, the lender, your credit and the loan&rsquo;s terms.</p>
<p>&ldquo;In general, a fixed-rate loan is a better long-term option for financing your education,&rdquo; says Fleischman. &ldquo;You are able to plan for future payments without worrying that interest rates may increase payments faster than your income increases.&rdquo;</p>
<p><strong>Variable-rate loans. </strong>The same factors that may determine your interest rate with a fixed-rate private student loan can impact your initial interest rate when you take out a variable-rate loan. However, with variable-rate loans, your interest rate may rise or fall in the future.</p>
<p>Variable-rate loans&rsquo; interest rates are tied to an index, such as the prime rate. The lender adds a margin to the index to determine your total interest rate. There may be a limit to how high or low your interest rate can go.</p>
<p>Variable-rate student loans tend to start with a lower initial interest rate than fixed-rate loans and could remain lower. However, you&rsquo;re taking on risk because the loan&rsquo;s interest rate could rise, causing your monthly payment and total cost of borrowing to increase.</p>
<p>A variable-rate loan may be best for those who can quickly repay the loan, which will limit your risk, or those who can afford higher monthly payments if the interest rate rises.</p>
<p><strong>What Are the Drawbacks of Private Student Loans?</strong><br /> Private student loans can help cover a gap in funding that some students need to fill. However, private student loans have drawbacks when compared to federal student loans.</p>
<p><strong>Credit-based decision. </strong>Private student loan eligibility and terms depend on the applicant&rsquo;s credit. Without a creditworthy co-signer, many students may not be able to get approved, or may only be able to get a high interest rate.</p>
<p><strong>Risk for co-signers. </strong>Co-signers take on additional debt and risk when they add their name to a private student loan. Co-signing could affect one&rsquo;s ability to qualify for other loans, and if the student isn&rsquo;t able to make a payment, it could hurt the co-signer&rsquo;s credit. In some cases, the co-signer will be responsible for the debt even if the student dies or is permanently disabled.</p>
<p><strong>Potentially higher interest rates. </strong>In some cases, private student loans may offer lower interest rates than federal student loans, but that&rsquo;s not always the case.</p>
<p><strong>Interest rate accrual. </strong>With subsidized federal loans, the government will pay the interest while you&rsquo;re in school and when the loans are in deferment. With private student loans, you&rsquo;ll accrue interest during these periods.</p>
<p><strong>No guaranteed hardship options. </strong>&ldquo;The difference between unsubsidized loans and private loans is deeper than the accrual of interest,&rdquo; says Fleischman. &ldquo;Unsubsidized loans come with federally mandated periods of in-school deferment, forbearance opportunities and a variety of income-driven repayment options.&rdquo; Some private student loan lenders offer deferment or forbearance options, but they might not be as lenient or lengthy as your options with federal student loans.</p>
<p><strong>No forgiveness programs. </strong>There are several federal student loan <a href="https://studentaid.ed.gov/sa/repay-loans/forgiveness-cancellation">forgiveness and cancellation programs</a> that aren&rsquo;t available with private student loans.</p>
<p><strong>Shorter default period and little recourse.</strong> If you default on a private student loan, the entire loan balance becomes due immediately. Federal student loans default after 270 days of nonpayment, and when they do, you may have several options for <a href="https://studentaid.ed.gov/sa/repay-loans/default/get-out">getting your loans out of default</a>.</p>
<p>Private student loans can default after one missed payment. You may be able to repay the late balance and bring the account current before the lender charges it off, often around four to six months, depending on the lender. However, federal student loan programs can be much more forgiving.</p>
<p><strong>How Can You Choose the Best Private Student Loan?</strong><br /> Based on recommendations from the <a href="https://www.consumer.ftc.gov/articles/1028-student-loans">Federal Trade Commission</a>, the <a href="https://www.consumerfinance.gov/students/">Consumer Financial Protection Bureau</a>, the <a href="https://studentaid.ed.gov/sa/types/loans/federal-vs-private">U.S. Department of Education</a> and thousands of consumer reviews, there are four key areas you should focus on when comparing private student loan lenders:</p>
<ol>
<li>Product offerings</li>
<li>Eligibility</li>
<li>Cost</li>
<li>Additional features</li>
</ol>
<p><strong>Product Offerings</strong></p>
<p>Once you&rsquo;ve determined the type of student loan you&rsquo;ll need, and about how much you want to borrow, check to see that the lenders&rsquo; offerings match your requirements. You can then compare their loan terms and limits to narrow down your list. For example, make sure the lender offers financing for your degree type.</p>
<p><strong>Eligibility</strong></p>
<p>Research lender eligibility requirements including citizenship status, enrollment, income and credit history. You should make sure you&rsquo;re likely to qualify for a student loan before you apply. Student loan eligibility requirements typically include:</p>
<ul>
<li><strong>Citizenship status. </strong>Private student loans are generally only available to U.S. citizens, U.S. nationals and permanent resident aliens. International students may be eligible if they have a U.S. citizen, national or permanent resident alien co-sign the loan.</li>
<li><strong>Enrollment. </strong>Lenders may only offer loans to students who are enrolled at least half time within an eligible school.</li>
<li><strong>Age. </strong>You must meet the age of majority in your state of residence or have an eligible co-signer.</li>
<li><strong>Income. </strong>There may be income requirements, including debt-to-income ratio requirements, that you or your co-signer must meet.</li>
<li><strong>Credit history. </strong>With private student loans, your credit history and score can determine your eligibility for a private loan and the interest rate you&rsquo;ll receive. If you don&rsquo;t have good credit or haven&rsquo;t yet established any credit, you may need to have a creditworthy co-signer, such as a parent or other trusted relative. Your co-signer&rsquo;s credit will be considered with your application. This makes the co-signer legally responsible for the student loan.</li>
</ul>
<p><strong>Cost</strong></p>
<p>The cost of your private student loan will depend on a variety of factors, including your interest rate and the type of interest you choose. Look closely at fees to calculate how they&rsquo;ll impact your total cost of borrowing.</p>
<p>Some lenders offer preapprovals, which will give you an estimated interest rate without hurting your credit. It&rsquo;s worth getting a preapproval if it&rsquo;s an option, as you can reliably find out the interest rate you&rsquo;ll be offered by each lender.</p>
<p>Lenders often have fees for applying or originating your loan. Not all lenders charge these, but you should always read the loan terms closely to identify potential fees, such as:</p>
<ul>
<li><strong>Application fee. </strong>The lender may charge a non-refundable fee to process your application.</li>
<li><strong>Origination fee. </strong>Origination fees, sometimes called disbursement fees, aren&rsquo;t common on private student loans. If the lender charges one, it&rsquo;s usually a fee that&rsquo;s equal to a percentage of the amount you borrow.</li>
<li><strong>Late fee. </strong>A fee required if your monthly payment is late. It may be a percentage of the amount due with a maximum amount, such as $15 or $25.</li>
</ul>
<p><strong>Interest capitalization</strong> isn&rsquo;t a fee, but how and when your interest is capitalized (becoming part of your loan principal) will influence your loan&rsquo;s total cost.</p>
<p>Some lenders let you forgo loan payments during school and for the first several months after graduation. Interest accrues on your loan principal, and when your interest capitalizes, your principal increases. As a result, you&rsquo;ll accrue more interest each month.</p>
<p>Interest capitalization also happens if you stop making payments, but continue to accrue interest in the future, such as when you put your loans into forbearance or deferment or stop making payments.</p>
<p>One thing you don&rsquo;t have to worry about with student loans is a <strong>prepayment penalty. </strong>Unlike some other types of loans, such as a mortgage or personal loans, lenders aren&rsquo;t allowed to charge you a fee if you pay off your student loan early.</p>
<p><strong>Additional Features</strong></p>
<p>The fine print in private student loans can vary from one lender to another. Some benefits or features could make it easier to repay the loan, lower your interest rate or make a lender a better option for other reasons.</p>
<ul>
<li><strong>Autopay savings. </strong>Many lenders offer an interest rate discount if you sign up for autopay. The discount is often 0.25% or 0.50%, however, it may not take effect until you start making full principal and interest payments.</li>
<li><strong>Other savings opportunities. </strong>Some lenders offer a discount if you have another financial product with them, such as a loan or bank account.</li>
<li><strong>Early repayment options. </strong>Private student loans start to accrue interest as soon as they are dispersed. Some lenders have repayment plans that start while you&rsquo;re in school. Making interest-only payments, full payments or a fixed monthly payment will help lower your loan balance before you graduate.</li>
<li><strong>Deferment options. </strong>You might be able to defer making any payments while you&rsquo;re in school. Lenders may offer a grace period after you graduate or drop below half time and you won&rsquo;t need to make full payments until the grace period ends.</li>
<li><strong>Deferment due to financial hardship. </strong>You may be able to defer your student loan payments if you go back to school, join the military or can&rsquo;t afford payments due to another covered reason, such as a job loss.</li>
<li><strong>Discharge due to death or permanent disability. </strong>Find out whether your loan balance passes on to your estate or co-signer if you die before it&rsquo;s repaid. Also, find out what happens if you become permanently disabled and can&rsquo;t afford to repay the debt.</li>
<li><strong>Co-signer release. </strong>Lenders may release the co-signer from the loan after the student makes a series of on-time payments and if he or she qualifies to take on the loan.</li>
</ul>
<p><strong>How Can You Get a Private Student Loan?</strong></p>
<p><br /> You&rsquo;ll go through a few major steps to obtain a student loan. When you apply, you&rsquo;ll need to meet the eligibility requirements, provide documentation and go through processing before approval and disbursement.</p>
<ol>
<li><strong> Eligibility: </strong>The lender will verify basic eligibility for the loan, including citizenship status and enrollment. With further documentation, your income, credit history and other eligibility factors will be verified.</li>
<li><strong> Required documentation: </strong>You&rsquo;ll need to provide personal and financial information when you apply for a private student loan. Having documentation available ahead of time could make your application process easier.</li>
</ol>
<p>Documentation required by lenders may include:</p>
<ul>
<li>Your name, address, phone number and email address</li>
<li>Your date of birth and Social Security number</li>
<li>A recent pay stub or other form of proof of income</li>
<li>Bank account balances</li>
<li>Your monthly housing payment (rent or mortgage)</li>
<li>Your employer&rsquo;s name, phone number and length of employment (if applicable)</li>
<li>Your school&rsquo;s name and estimated cost of attendance</li>
<li>Your year in school and period of enrollment</li>
<li>The amount of financial aid you&rsquo;ve received (you can find this on the award letter from your school)</li>
<li>Your anticipated graduation date, loan period and the loan amount needed</li>
<li>References</li>
<li>Co-signer name and valid contact information (if applicable). Your co-signer may need to share a lot of the same information with the lender.</li>
</ul>
<ol start="3">
<li><strong> Processing: </strong>Many private student loan lenders let you submit your application online. You may get a decision back within a few minutes after the lender analyzes your credit, finances and other eligibility criteria. Or, you may need to submit additional supporting documents or information if the lender has any questions.</li>
<li><strong> Approval and disbursement: </strong>Once you&rsquo;re approved for a private student loan, you can decide on the interest rate type, loan term and repayment plan, then accept the terms of the loan and sign the loan agreement.</li>
</ol>
<p>The lender will contact your school to verify that you&rsquo;re eligible for the loan amount you requested. Depending on the school, it could take about two to five weeks for the lender to hear back. The school then schedules the disbursement dates and amounts for the loan.</p>
<p>Private student loans will be sent directly to the school. If your loan amount exceeds what you owe the school for that semester, you may receive a refund for the difference. You could return your refund to the lender, lowering your loan amount, or you can spend it on education-related expenses, such as room, board or books.</p>
 

                        </div>

                </div>

                <aside class="col-md-4">

                        <div class="sidebar-content sticky-sidebar">

                                <div class="sticky-side-menu">

                                        <h4>Blogs</h4>

                                        <ul>

                                                <a href="javascript:void(0);"><li>Paying for Your College</li></a>

                                                <a href="how-to-save-money-by-refinancing.php"><li>How To Save Money By Refinancing?</li></a>

                                        </ul>

                                </div>

                                <div class="special-offer">

                                        <img src="images/offer-1.png" alt="Offer" />

                                     <!--    <h4>Get 10% Off</h4> -->

                                        <a href="student-registration.php"><button type="button" class="btn-apply-inner">Apply Now</button></a>

                                </div>

                        </div>

                </aside>

        </div>

</div>

<?php include "footer.html" ?>

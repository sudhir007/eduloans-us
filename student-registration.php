<?php include "header.html" ?>
<div class="container">
        <div class="row">
                <div class="col-md-12">
                        <div class="page-intro">
                                <p class="my-breadcrumbs">Home / Student's Registration</p>
                                <h1>Student's Registration</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque eius expedita libero, voluptatum saepe. Tempora, nam temporibus totam maxime odit quasi eos voluptatem atque corporis cupiditate adipisci, itaque ducimus quam.</p>
                        </div>
                </div>
        </div>
        <div class="row my-shadow-effect">
                <form action="" class="contact-form">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                                
                                <h2 class="text-center my-section-header">Personal Information</h2>
                                <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="Name">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" id="date" name="date" placeholder="DOB (dd/mm/yyyy)">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="email" class="form-control element-block" placeholder="Email">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="tel" class="form-control element-block" placeholder="Phone No.">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="No. of years at the address">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                        <textarea class="form-control element-block" placeholder="Address"></textarea>
                                                </div>
                                        </div>
                                        
                                </div>
                                
                                
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                                
                                <h2 class="text-center my-section-header">School Information</h2>
                                <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="Name of School">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="Course">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="I20 Value and upload">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="Duration of Program">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="Financial Aid provided by the school">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                        <input type="text" class="form-control element-block" placeholder="Any other financial Aid provided">
                                                </div>
                                        </div>
                                        
                                </div>
                                
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="text-center">
                                        <button type="submit" class="btn-submit-contact">Submit</button>
                                </div>
                        </div>
                </form>
        </div>
</div>
<?php include "footer.html" ?>
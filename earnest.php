<?php include "header.html" ?>

<div class="container">

        <div class="row">

                <div class="col-md-12">

                        <div class="page-intro">

                                <p class="my-breadcrumbs">Partner with us / Earnest</p>

                                <h1>Earnest</h1>

                                <p>Earnest is a full-service student loan company that offers education loans based on people unique financial profile. Founded in 2013, Earnest provides loans to help students achieve their dreams for quality education. </p>

<p>The company offers student loan refinancing, private student loans, and personal loans at potentially lower interest rates. The borrowers get more options to customize their loan payments and terms. The San Francisco-based company, Earnest has lent nearly $2 billion in student loans. 

                                </p>

                                

                        </div>

                </div>

        </div>

        <div class="row">

                <div class="col-md-12">

                        <div class="inner-main-content-holder">

                                <h2>Why choose Earnest for student loans </h2>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Covers up to 100% of school’s certified cost of attendance</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Easy application & quick approval process</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Competitive fixed and variable interest rates</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> No origination, disbursement, prepayment or late fees</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Flexible 5-year to 20-year repayment options</div>

                                <div class="my-marginer"><i class="fas fa-book my-text-color"></i> Instant support from customer service team </div>



                        </div>

                </div>

        </div>

</div>

<div class="inner-middle-bg">

        <h4>Earnest gives student loans that fit your budget</h4>

        <button type="button" class="btn-apply-inner">Get Loan</button>

</div>


<?php include "table-two.php" ?>

<?php include "footer.html" ?>
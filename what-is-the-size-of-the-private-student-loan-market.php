<?php include "header.html" ?>

<div class="container">
        <div class="row">
                <div class="col-md-12">
                        <div class="blog-page-intro blog-img-1">
                        </div>
                </div>
        </div>

        <div class="row">
                <h1 class="text-center">Traditional Lenders Need To Compete In A Digital Banking Marketplace </h1>

                <div class="col-md-8">

 <div class="inner-main-content-holder">
                              
<h5>Private Student Loan Statistics</h5>
<ul>
<li> $123.14 billion: the total amount of private student loan debt (source) </li>
<li> 7.71%: the percentage of total outstanding student debt that is from private student loans (source) </li>
<li> $13,780: the average private student loan amount (source) </li>
<li> 8.86%: the average interest rate on private student loans (source) </li>
<li> 8.81%: average variable rate </li>
<li> 9.97%: average fixed rate </li>
<li> 21.04%: the average approval rate for all private student loan applications (source) </li>
<li> 40.51%: average approval rate with a cosigner </li>
<li> 8.66%: average approval rate without a cosigner </li>
<li> 737: the average credit score of approved private student loan applicants (source) </li>
<li> $72,947: the average income of approved private student loan applicants (source) </li>
<li> 2.48%: percent of private student loans that are 30 to 89 days past due (source) </li>
<li> 1.50%: percent of private student loans that are 90+ days past due (source) </li>
<li> Over 100: the number of private student loan lenders </li>
</ul>
 

                        </div>

                </div>

                <aside class="col-md-4">

                        <div class="sidebar-content sticky-sidebar">

                                <div class="sticky-side-menu">

                                        <h4>Blogs</h4>

                                        <ul>

                                                <a href="javascript:void(0);"><li>Paying for Your College</li></a>

                                                <a href="how-to-save-money-by-refinancing.php"><li>How To Save Money By Refinancing?</li></a>

                                        </ul>

                                </div>

                                <div class="special-offer">

                                        <img src="images/offer-1.png" alt="Offer" />

                                     <!--    <h4>Get 10% Off</h4> -->

                                        <a href="student-registration.php"><button type="button" class="btn-apply-inner">Apply Now</button></a>

                                </div>

                        </div>

                </aside>

        </div>

</div>

<?php include "footer.html" ?>
